![Applaudo Studios](https://applaudostudios.com/static/full_logo-d699ecc3a0afe89470e36657d866f405.svg "Applaudo Studios")
# Applaudo Campus 

Application created to automate the information management processes of the trainee programs: evaluations and content. Both for developers and trainers as well as for trainees.

[Project link](https://campus-applaudo.vercel.app/)

## Features

- **Trainee:** Watch all you grades and progress on the trainee program
- **Trainer:** Evaluate trainees, get programs, modules and items information.
- **Developer:** Manage content to trainee programs: modules, assignments and items.

## Developers

- Vicente De Paz
- Emerson Nolasco
- Ernesto Hernández
