import React from 'react';

import { Avatar, Box, Card, CardContent, Grid, LinearProgress } from '@material-ui/core';

import IEmployee from 'interfaces/IEmployee';

import styles from './styles.module.scss';

interface Props {
  title: string;
  state: string;
  progress: number;
  description?: string;
  trainers?: Array<IEmployee>;
}

const ExecutionOverview = ({
  title,
  state,
  progress,
  description,
  trainers,
}: Props): JSX.Element => {
  return (
    <Box px={2}>
      <Grid container spacing={3} className={styles.header}>
        <Grid item xs={12} md={6}>
          <h1>{title}</h1>
          <i>{state}</i>
          <Box display="flex" alignItems="center" my={2}>
            <Box width="100%" mr={1}>
              <LinearProgress variant="determinate" value={progress} />
            </Box>
            <Box minWidth={35}>{`${progress} %`}</Box>
          </Box>
        </Grid>
      </Grid>

      {description && (
        <Grid container>
          <Card>
            <CardContent>
              <h2>About this course</h2>
              <p>{description}</p>
            </CardContent>
          </Card>
        </Grid>
      )}

      {trainers?.length ? (
        <>
          <h2>Trainers</h2>
          <div className={styles.avatarGroup}>
            {trainers.map((trainer) => (
              <a className={styles.avatar} key={trainer.id} href={`mailto:${trainer.email}`}>
                <Avatar alt={trainer.firstNames} className={styles.avatarIcon} src="noimage" />
              </a>
            ))}
          </div>
        </>
      ) : null}
    </Box>
  );
};

ExecutionOverview.defaultProps = {
  description: null,
  trainers: null,
};

export default ExecutionOverview;
