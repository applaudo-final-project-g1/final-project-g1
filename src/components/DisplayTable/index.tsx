import React from 'react';

import { TableRow, TableCell } from '@material-ui/core';

type Props<T> = {
  items: T[];
};

const DisplayTable = <T,>({ items }: Props<T>): JSX.Element => {
  return (
    <>
      {items.map((row) => (
        <TableRow key={Object.values(row)[0]}>
          {Object.entries(row).map((info) => (
            <TableCell key={info[1]} align="center">
              {info[1]}
            </TableCell>
          ))}
        </TableRow>
      ))}
    </>
  );
};

export default DisplayTable;
