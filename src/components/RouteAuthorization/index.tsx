/* eslint-disable no-continue */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { ReactNode } from 'react';

import useUser from 'hooks/useUser';
import { RolesEnum } from 'interfaces/IEmployee';
import rules, { ModuleEnum } from 'navigation/rbacRules';
import Routes from 'navigation/Routes';

type Props = {
  yes: ReactNode;
  no: ReactNode;
  route: Routes;
};

const check = (roles: RolesEnum[], route: Routes) => {
  for (let i = 0; i < roles.length; i += 1) {
    const permissions = rules[roles[i]];
    if (!permissions) {
      // role is not present in the rules
      continue;
    }

    const routesPermissions = permissions[ModuleEnum.routes];

    if (routesPermissions && routesPermissions.includes(route)) {
      // static rule not provided for action
      return true;
    }
  }

  return false;
};

const RouteAuthorization = ({ yes, no, route }: Props): JSX.Element => {
  const [user] = useUser();
  return check(user.roles, route) ? <>{yes}</> : <>{no}</>;
};

export default RouteAuthorization;
