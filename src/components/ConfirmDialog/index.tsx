import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

interface Props {
  open: boolean;
  title: string;
  textContent: string;
  handleConfirm?: () => void;
  handleCancel?: () => void;
}

const ConfirmDialog = ({
  open,
  title,
  textContent,
  handleConfirm,
  handleCancel,
}: Props): JSX.Element => {
  return (
    <div>
      <Dialog open={open} onClose={handleCancel} aria-labelledby="responsive-dialog-title">
        <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{textContent}</DialogContentText>
        </DialogContent>
        <DialogActions>
          {handleCancel && (
            <Button autoFocus onClick={handleCancel} color="primary">
              Cancel
            </Button>
          )}
          {handleConfirm && (
            <Button onClick={handleConfirm} color="primary" autoFocus>
              Accept
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </div>
  );
};

ConfirmDialog.defaultProps = {
  handleConfirm: null,
  handleCancel: null,
};

export default ConfirmDialog;
