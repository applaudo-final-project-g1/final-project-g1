import React, { useEffect, useState } from 'react';

import { Box, Button, CircularProgress } from '@material-ui/core';
import { Redirect, useHistory } from 'react-router-dom';
import useSWR, { mutate } from 'swr';

import fetcher from 'api/axiosFetcher';
import { finish } from 'api/gradeService';
import ConfirmDialog from 'components/ConfirmDialog';
import GradeCard from 'components/GradeCard';
import ErrorHandler from 'components/RequestErrorHandler';
import { IEvaluation } from 'interfaces/IEvaluation';
import IEvaluationScale from 'interfaces/IEvaluationScale';
import IItem from 'interfaces/IItem';
import Routes from 'navigation/Routes';
import { calculateItemGrade, convertGradeToBaseTen } from 'utils/utils';

import styles from './styles.module.scss';

interface EvaluationInfo {
  id: number;
  attachment: string | null;
  date: string | null;
  finishedAt: string | null;
  grade: number | null;
  assignment: IAssignment;
  grades: IGrade[];
}

interface IAssignment {
  id: number;
  label: string;
  weight: number;
  canSubmit: boolean;
  assignmentItems: IItem[];
}

interface IGrade {
  id: number;
  weight: number;
  feedback: string;
  scaleId: number;
  assignmentItemId: number;
}

type Props = {
  evaluation: IEvaluation;
};

const Evaluate = ({ evaluation }: Props): JSX.Element => {
  const history = useHistory();

  const {
    data: evaluationData,
    isValidating: isValidatingEvaluation,
    error: errorEvaluation,
  } = useSWR<EvaluationInfo>(`/evaluations/${evaluation.id}`, fetcher);

  const {
    data: evaluationScalesData,
    error: evaluationScalesError,
    isValidating: evaluationsScalesLoading,
  } = useSWR<IEvaluationScale[]>('/evaluation-scales', fetcher);

  const [open, setOpen] = useState(false);
  const [loadingFinish, setLoadingFinish] = useState(false);
  const [errorFinish, setErrorFinish] = useState('');
  const [grade, setGrade] = useState(0);

  const { trainee } = evaluation;

  const handleBack = () => {
    if (history.length <= 2) history.push(Routes.HOME);
    else history.goBack();
  };

  const openModal = () => setOpen(true);

  const handleCancelFinish = () => {
    setOpen(false);
  };

  const onFinish = async () => {
    try {
      setLoadingFinish(true);
      setErrorFinish('');
      await finish(evaluation.id);
      setOpen(false);
      setLoadingFinish(false);
      handleBack();
    } catch (e) {
      setErrorFinish('Could finish this evaluation 😥');
      setLoadingFinish(false);
    }
  };

  const changeGrade = () => {
    mutate(`/evaluations/${evaluation.id}`);
  };

  const getGradePercentage = (id: number) => {
    if (evaluationScalesData) {
      const scale = evaluationScalesData.find((e) => e.id === id);
      return scale ? scale.percentage : '0';
    }

    return '0';
  };

  useEffect(() => {
    if (evaluationData && evaluationScalesData) {
      /*
        Grade calculation
        Calculate current grade using the individual items' grades of the evaluation
        Every assignment has a base that is the addition of all the items' weights
        it is not necesary to be 10, however the grade is indeed everytime base 10
        so, the final grade is converted from the actual base to base 10
      */
      const { assignmentItems } = evaluationData.assignment;
      const { grades } = evaluationData;
      const base = assignmentItems.reduce((prev, curr) => prev + curr.weight, 0);
      let basedGrade = 0;
      for (let i = 0; i < grades.length; i += 1) {
        const currentGrade = grades[i];
        basedGrade += calculateItemGrade(
          currentGrade.weight,
          getGradePercentage(currentGrade.scaleId)
        );
      }
      setGrade(convertGradeToBaseTen(basedGrade, base));
    }
  }, [evaluationData, evaluationScalesData]);

  if (
    (errorEvaluation && !isValidatingEvaluation) ||
    (evaluationScalesError && !evaluationsScalesLoading)
  )
    return <ErrorHandler errorCode={errorEvaluation || evaluationScalesError} />;

  if (evaluation.finishedAt) return <Redirect to={Routes.HOME} />;

  return (
    <Box mt={8} width="100%">
      {isValidatingEvaluation && !evaluationData && (
        <Box display="flex" justifyContent="center">
          <CircularProgress color="primary" />
        </Box>
      )}
      {errorEvaluation && (
        <>
          <p className={styles.error}>Could not load assignment data</p>
          <Box display="flex" justifyContent="center">
            <Button onClick={handleBack}>Go back</Button>
          </Box>
        </>
      )}
      {evaluationData && (
        <>
          <Box display="flex" alignContent="center">
            <Box alignSelf="flex-start" flexGrow={1}>
              <h2>{evaluationData.assignment.label}</h2>
            </Box>
            <Box alignSelf="flex-end">
              <h4>Grade: {grade}</h4>
            </Box>
          </Box>
          <Box display="flex" justifyContent="space-between">
            <Button onClick={handleBack}>Go back</Button>
          </Box>
          <Box>
            <p>
              <b>Trainee:</b> {trainee.email}
            </p>
          </Box>
          {evaluationData.assignment.canSubmit && evaluation.attachment && (
            <Box display="flex" justifyContent="center" m={2}>
              <a href={evaluation.attachment} className={styles.homeworkLink}>
                <Button type="button" variant="contained" color="primary">
                  Homework link
                </Button>
              </a>
            </Box>
          )}
          {evaluationsScalesLoading && !evaluationData && (
            <Box display="flex" justifyContent="center" m={4}>
              <CircularProgress color="primary" />
            </Box>
          )}
          {evaluationScalesError && <p className={styles.error}>Could not load assignment items</p>}
          {evaluationData && evaluationScalesData && (
            <>
              {evaluationData.assignment.assignmentItems.map((item) => {
                const { grades } = evaluationData;
                const itemGrade = grades.find((e) => e.assignmentItemId === item.id);
                return (
                  <GradeCard
                    key={item.id}
                    item={item}
                    evaluationScales={evaluationScalesData}
                    evaluationId={evaluation.id}
                    changeGrade={changeGrade}
                    rootFeedback={itemGrade && itemGrade.feedback}
                    scaleId={itemGrade && itemGrade.scaleId}
                  />
                );
              })}
              <Box display="flex" justifyContent="center" m={2}>
                <Button
                  type="button"
                  variant="contained"
                  color="primary"
                  onClick={openModal}
                  disabled={loadingFinish}
                >
                  Finish evaluation
                </Button>
              </Box>
              {loadingFinish && (
                <Box display="flex" justifyContent="center">
                  <CircularProgress color="primary" />
                </Box>
              )}
              {errorFinish && <p>{errorFinish}</p>}
              <ConfirmDialog
                open={open}
                title="Confirm action"
                textContent="Do you want to finish this evaluation?"
                handleConfirm={onFinish}
                handleCancel={handleCancelFinish}
              />
            </>
          )}
        </>
      )}
    </Box>
  );
};

export default Evaluate;
