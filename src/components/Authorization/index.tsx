import React, { ReactNode } from 'react';

import useUser from 'hooks/useUser';
import { RolesEnum } from 'interfaces/IEmployee';

type Props = {
  children: ReactNode;
  notAuthorizedComponent?: ReactNode;
  roles: RolesEnum[];
};

const Authorization = ({ children, notAuthorizedComponent, roles }: Props): JSX.Element | null => {
  const [user] = useUser();

  if (user.roles.filter((rol) => roles.includes(rol)).length > 0) return <>{children}</>;

  return notAuthorizedComponent ? <>{notAuthorizedComponent}</> : null;
};

Authorization.defaultProps = {
  notAuthorizedComponent: undefined,
};

export default Authorization;
