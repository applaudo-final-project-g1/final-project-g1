import React from 'react';

import { CircularProgress } from '@material-ui/core';

import Styles from './styles.module.scss';

const Loading = (): JSX.Element => {
  return (
    <div className={Styles.loadingContainer}>
      <CircularProgress color="primary" />
    </div>
  );
};

export default Loading;
