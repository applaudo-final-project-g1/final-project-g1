import React, { useState } from 'react';

import { Box, Button, CircularProgress, Container, CssBaseline, TableRow } from '@material-ui/core';
import useSWR, { mutate } from 'swr';

import { generateEvaluations } from 'api/assignmentService';
import fetcher from 'api/axiosFetcher';
import ClickableTable from 'components/ClickableTable';
import ConfirmDialog from 'components/ConfirmDialog';
import DisplayTable from 'components/DisplayTable';
import FooterCell from 'components/FooterCell';
import ErrorHandler from 'components/RequestErrorHandler';
import WrapperTable from 'components/WrapperTable';
import { IAssignmentWithEvaluations } from 'interfaces/IAssignment';
import IItem from 'interfaces/IItem';

interface Props {
  assignmentId: string;
}

const itemsHeadersInterview = ['Questions', 'Weight'];
const itemsHeadersHomework = ['Required Task', 'Weight'];
const evaluationsHeaders = ['Trainee', 'Date', 'Grades'];

const AssignmentTrainer = ({ assignmentId }: Props): JSX.Element => {
  const [modal, setModal] = useState(false);
  const [loadingEvals, setLoading] = useState(false);

  const {
    data: evaluationData,
    error: evaluationError,
    isValidating: evaluationValidating,
  } = useSWR<IAssignmentWithEvaluations>(`/assignments/${assignmentId}/evaluations`, fetcher);

  const { data: itemsData, error: itemsError, isValidating: itemsValidating } = useSWR<IItem[]>(
    `/assignments/${assignmentId}/items`,
    fetcher
  );

  const handleGenerate = async () => {
    setLoading(true);
    const evaluations = await generateEvaluations(assignmentId);
    mutate(`/assignments/${assignmentId}/evaluations`);
    if (!evaluations.length) setModal(true);
    setLoading(false);
  };

  if ((evaluationError && !evaluationValidating) || (itemsError && !itemsValidating))
    return <ErrorHandler errorCode={evaluationError.message || itemsError.message} />;

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      {(!itemsData || !evaluationData) && <CircularProgress color="primary" />}

      {evaluationData && itemsData && (
        <>
          <h1>{evaluationData.label}</h1>
          <WrapperTable
            headers={evaluationData.canSubmit ? itemsHeadersHomework : itemsHeadersInterview}
            ariaText="table-for-items"
          >
            <DisplayTable
              items={itemsData.map((item) => ({
                item: item.description,
                percentage: `${item.weight}`,
              }))}
            />
            <TableRow>
              <FooterCell>Total</FooterCell>
              <FooterCell>{`${itemsData.reduce((acc, curr) => acc + curr.weight, 0)}`}</FooterCell>
            </TableRow>
          </WrapperTable>
          <Box display="flex" justifyContent="space-between" p={3}>
            <h2>
              {evaluationData.canSubmit
                ? `Submitted Assignments (${evaluationData.evaluations.length})`
                : `Interview (${evaluationData.evaluations.length})`}
            </h2>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleGenerate}
              disabled={loadingEvals}
            >
              <b>Generate evaluations</b>
            </Button>
          </Box>
          <WrapperTable headers={evaluationsHeaders} ariaText="table-for-evaluations">
            <ClickableTable
              pathTo={`/assignments/${evaluationData.id}/evaluations`}
              items={evaluationData.evaluations.map((evaluation) => ({
                id: evaluation.id,
                trainee: evaluation.trainee.email,
                date: evaluation.date ? evaluation.date.toLocaleDateString() : 'Not Submitted',
                grades: evaluation.grade ? evaluation.grade.toString() : 'Not Graded',
                clickable: !evaluation.finishedAt,
                payload: evaluation,
              }))}
            />
          </WrapperTable>
          <ConfirmDialog
            open={modal}
            title="Warning"
            textContent="All evaluations have already been created"
            handleConfirm={() => setModal(false)}
          />
        </>
      )}
    </Container>
  );
};

export default AssignmentTrainer;
