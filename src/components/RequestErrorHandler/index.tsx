import React, { useEffect } from 'react';

import { Redirect } from 'react-router-dom';

import useUser from 'hooks/useUser';
import { IUser } from 'interfaces/IUser';
import Routes from 'navigation/Routes';

type Props = {
  errorCode: string;
};

const ErrorHandler = ({ errorCode }: Props): JSX.Element => {
  const [user, setUser] = useUser();

  useEffect(() => {
    if (errorCode === '401') {
      setUser({} as IUser);
    }
  }, []);

  return (
    <>
      {errorCode === '401' && user.jwt.length > 0 && (
        <Redirect exact to={{ pathname: Routes.LOGIN, state: { expired: true } }} />
      )}
      {errorCode === '400' && 'Invalid information'}
      {errorCode === '404' && 'Resource not found'}
      {errorCode === '500' && 'Something went wrong'}
    </>
  );
};

export default ErrorHandler;
