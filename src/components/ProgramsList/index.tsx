import React from 'react';

import { Box, Card, CardContent, CircularProgress } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ExecutionOverview from 'components/Execution';
import ErrorHandler from 'components/RequestErrorHandler';
import IProgram from 'interfaces/IProgram';
import { calculateProgramProgress } from 'utils/utils';

import styles from './styles.module.scss';

const ProgramsList = (): JSX.Element => {
  const history = useHistory();
  const { data, error, isValidating } = useSWR<IProgram[]>('/programs', fetcher);
  const handleProgramClick = (programId: number) => history.push(`/programs/${programId}`);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;

  return (
    <Box mt={8} width="100%">
      <h1>Programs</h1>
      {!data && (
        <Box width="100%" display="flex" justifyContent="center">
          <CircularProgress />
        </Box>
      )}
      {data?.map((program) => {
        const progress = calculateProgramProgress(program);
        return (
          <Card className="verticalMargin" key={program.name}>
            <CardContent
              onClick={() => handleProgramClick(program.id)}
              className={styles.cursorPointer}
            >
              <ExecutionOverview
                title={program.name}
                description={program.description}
                progress={progress < 100 ? progress : 100}
                state={progress < 100 ? 'In progress' : 'Completed'}
              />
            </CardContent>
          </Card>
        );
      })}
    </Box>
  );
};

export default ProgramsList;
