import React from 'react';

import { Button } from '@material-ui/core';

import useThemeProvider from 'hooks/useTheme';

const Toggle = (): JSX.Element => {
  const [theme, setTheme] = useThemeProvider();

  const toggleTheme = () => {
    setTheme(theme.palette.type === 'light' ? 'dark' : 'light');
  };

  return (
    <Button onClick={toggleTheme} variant="contained" color="primary">
      Theme
    </Button>
  );
};

export default Toggle;
