import React, { useState } from 'react';

import { yupResolver } from '@hookform/resolvers/yup';
import {
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

import grade from 'api/gradeService';
import IEvaluationScale from 'interfaces/IEvaluationScale';
import IItem from 'interfaces/IItem';

import useSelectStyles from './selectStyles';
import styles from './styles.module.scss';

interface IFormInputs {
  evaluationScaleId: string;
  feedback: string;
}

const schema = yup.object().shape({
  feedback: yup.string(),
  evaluationScaleId: yup.string().required('Evaluation scale is a required field'),
});

type Props = {
  item: IItem;
  rootFeedback?: string;
  scaleId?: number;
  evaluationScales: IEvaluationScale[];
  evaluationId: number;
  changeGrade(): void;
};

const GradeCard = ({
  item,
  rootFeedback,
  scaleId,
  evaluationScales,
  evaluationId,
  changeGrade,
}: Props): JSX.Element => {
  const [isLoading, setIsLoading] = useState(false);
  const [saved, setSaved] = useState(false);
  const [error, setError] = useState('');

  const { description } = item;

  const classes = useSelectStyles();
  const { handleSubmit, control, errors } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
    shouldUnregister: false,
    defaultValues: {
      evaluationScaleId: scaleId ? scaleId.toString() : '',
      feedback: rootFeedback ?? '',
    },
  });

  const onSubmit = async (data: IFormInputs) => {
    try {
      setIsLoading(true);
      setError('');
      const { evaluationScaleId, feedback } = data;
      await grade(evaluationId, {
        assignmentItemId: item.id,
        evaluationScaleId: parseInt(evaluationScaleId, 10),
        feedback,
      });
      changeGrade();
      setIsLoading(false);
      setSaved(true);
    } catch (e) {
      setError('Could not save grade');
      setIsLoading(false);
    }
  };

  return (
    <Card className={styles.card}>
      <CardContent>
        <Typography variant="h6" color="primary" component="p" align="center">
          {description}
        </Typography>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Box className={classes.textArea}>
            <Controller
              control={control}
              as={TextField}
              name="feedback"
              label="Feedback"
              placeholder="write feedback if necessary"
              margin="normal"
              error={!!errors.feedback}
              helperText={errors.feedback?.message}
              multiline
              rows={2}
              rowsMax={4}
              fullWidth
            />
          </Box>
          <FormControl className={classes.formControl}>
            <InputLabel id="evaluation-scale-select-label">Evaluation scale</InputLabel>
            <Controller
              control={control}
              name="evaluationScaleId"
              as={
                <Select
                  id="evaluation-scale-select"
                  labelId="evaluation-scale-select-label"
                  error={!!errors.evaluationScaleId}
                  fullWidth
                >
                  <MenuItem value="">Select a scale</MenuItem>
                  {evaluationScales.map((evaluation) => (
                    <MenuItem key={evaluation.id} value={evaluation.id}>
                      {evaluation.label}
                    </MenuItem>
                  ))}
                </Select>
              }
            />
            <FormHelperText>{errors.evaluationScaleId?.message}</FormHelperText>
          </FormControl>
          <Box display="flex" justifyContent="center" mt={5}>
            <Button type="submit" variant="contained" color="primary" disabled={isLoading}>
              Save
            </Button>
          </Box>
          <div className={styles.cardMessages}>
            {isLoading && <CircularProgress color="primary" />}
            {error && <p>{error}</p>}
            {!isLoading && saved && <p className={styles.saved}>Grade saved!</p>}
          </div>
        </form>
      </CardContent>
    </Card>
  );
};

GradeCard.defaultProps = {
  rootFeedback: undefined,
  scaleId: undefined,
};

export default GradeCard;
