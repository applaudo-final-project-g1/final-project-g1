import { makeStyles, Theme } from '@material-ui/core';

const useSelectStyles = makeStyles((theme: Theme) => ({
  formControl: {
    maxWidth: 350,
    width: '100%',
    [theme.breakpoints.up('md')]: {
      marginLeft: '80px',
      marginRight: '80px',
    },
  },
  textArea: {
    [theme.breakpoints.up('md')]: {
      marginLeft: '80px',
      marginRight: '80px',
    },
  },
}));

export default useSelectStyles;
