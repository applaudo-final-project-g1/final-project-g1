import React from 'react';

import { Box, Avatar } from '@material-ui/core';

import IEmployee from 'interfaces/IEmployee';

import useProfileInfoStyles from './profileInfoStyles';

type Props = {
  info: IEmployee;
};

const ProfileInfo = ({ info }: Props): JSX.Element => {
  const { email, firstNames, lastNames } = info;
  const classes = useProfileInfoStyles();

  return (
    <Box display="flex" flexDirection="row" className={classes.root}>
      <Box display="flex" className={classes.col}>
        <Avatar className={classes.icon}>{email[0].toUpperCase()}</Avatar>
        <span>{email}</span>
      </Box>
      <Box display="flex" className={classes.col}>
        <div>
          <span>{firstNames ?? 'Name not available'}</span>
          <p>Firstname</p>
        </div>
        <div>
          <span>{lastNames ?? 'Name not available'}</span>
          <p>Lastname</p>
        </div>
      </Box>
    </Box>
  );
};

export default ProfileInfo;
