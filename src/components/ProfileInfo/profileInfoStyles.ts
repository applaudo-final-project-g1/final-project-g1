import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useProfileInfoStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      fontSize: theme.spacing(2.5),
      padding: theme.spacing(4),
      justifyContent: 'space-evenly',
      [theme.breakpoints.down('sm')]: {
        flexDirection: 'column',
        padding: theme.spacing(0),
      },
    },
    icon: {
      backgroundColor: theme.palette.info.main,
      width: theme.spacing(25),
      height: theme.spacing(25),
      fontSize: theme.spacing(15),
      marginBottom: theme.spacing(4),
    },
    col: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      '& > div': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > p': {
          fontWeight: 'bold',
          fontSize: theme.spacing(2),
          align: 'center',
        },
        [theme.breakpoints.down('sm')]: {
          marginBottom: theme.spacing(7),
        },
      },
      [theme.breakpoints.down('sm')]: {
        marginBottom: theme.spacing(7),
      },
    },
  })
);

export default useProfileInfoStyles;
