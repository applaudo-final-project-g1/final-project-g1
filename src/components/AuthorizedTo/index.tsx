/* eslint-disable no-continue */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { ReactNode } from 'react';

import useUser from 'hooks/useUser';
import { RolesEnum } from 'interfaces/IEmployee';
import rules, { ModuleEnum, PermissionEnum } from 'navigation/rbacRules';

type Props = {
  yes: ReactNode;
  no?: ReactNode;
  module: ModuleEnum;
  permission: PermissionEnum;
};

const check = (roles: RolesEnum[], module: ModuleEnum, permission: PermissionEnum) => {
  for (let i = 0; i < roles.length; i += 1) {
    const permissions = rules[roles[i]];
    if (!permissions) {
      // role is not present in the rules
      continue;
    }

    if (module !== ModuleEnum.routes) {
      const modulePermissions = permissions[module];

      if (modulePermissions && modulePermissions.includes(permission)) {
        // static rule not provided for action
        return true;
      }
    }

    continue;
  }

  return false;
};

const AuthorizedTo = ({ yes, no, module, permission }: Props): JSX.Element => {
  const [user] = useUser();
  return check(user.roles, module, permission) ? <>{yes}</> : <>{no}</>;
};

AuthorizedTo.defaultProps = {
  no: null,
};

export default AuthorizedTo;
