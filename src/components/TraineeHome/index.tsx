import React, { useEffect, useState } from 'react';

import { CircularProgress } from '@material-ui/core';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ExecutionOverview from 'components/Execution';
import ModulesList from 'components/ModulesList';
import ErrorHandler from 'components/RequestErrorHandler';
import { IProgramWithModules } from 'interfaces/IProgram';
import { calculateProgramProgress } from 'utils/utils';

const TraineeHome = (): JSX.Element => {
  const { data, isValidating, error } = useSWR<IProgramWithModules>(
    '/trainees/me/programs',
    fetcher
  );

  const [progress, setProgress] = useState(0);
  const [programState, setProgramState] = useState<'In progress' | 'Completed'>('In progress');

  useEffect(() => {
    if (data) {
      const prog = calculateProgramProgress(data);
      setProgress(prog < 100 ? prog : 100);
      if (prog >= 100) setProgramState('Completed');
    }
  }, [data]);

  if (error && !isValidating) return <ErrorHandler errorCode={error} />;

  return (
    <>
      {!data && <CircularProgress />}
      {data && (
        <>
          <ExecutionOverview
            title={data.name}
            description={data.description}
            trainers={data.trainers}
            progress={progress}
            state={programState}
          />
          <div className="verticalMargin">
            <ModulesList modules={data.modules} programId={data.id.toString()} />
          </div>
        </>
      )}
    </>
  );
};

export default TraineeHome;
