import React, { useEffect, useState } from 'react';

import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button, Grid, TextField, DialogContent, DialogActions } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import { Controller, useForm } from 'react-hook-form';
import useSWR, { mutate } from 'swr';
import * as yup from 'yup';

import {
  addAssignmentItem,
  updateAssignmentItem,
  deleteAssignmentItem,
} from 'api/assigmentItemService';
import fetcher from 'api/axiosFetcher';
import Authorization from 'components/Authorization';
import ConfirmDialog from 'components/ConfirmDialog';
import ListItemCard from 'components/ListItemCard';
import ErrorHandler from 'components/RequestErrorHandler';
import { RolesEnum } from 'interfaces/IEmployee';
import IItem from 'interfaces/IItem';
import { ModuleEnum } from 'navigation/rbacRules';

interface Props {
  assignmentId: string;
}

interface IFormInputs {
  description: string;
  weight: number;
}

const schema = yup.object().shape({
  description: yup.string().required(),
  weight: yup.number().positive().required(),
});

const AssignmentItemList = ({ assignmentId }: Props): JSX.Element => {
  const [open, setOpen] = useState(false);
  const [openDelModal, setOpenDelModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState<IItem | null>(null);

  const { handleSubmit, control, errors, setValue, reset } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
    shouldUnregister: false,
  });

  const { data, error, isValidating } = useSWR<IItem[]>(
    `/assignments/${assignmentId}/items`,
    fetcher
  );

  const openDeleteModal = (e: React.MouseEvent<HTMLButtonElement>, id: number) => {
    e.preventDefault();
    const assignmentItem = data?.find((item) => item.id === id);
    if (assignmentItem) setSelectedItem(assignmentItem);
    setOpenDelModal(true);
  };

  const openEditModal = (e: React.MouseEvent<HTMLButtonElement>, id: number) => {
    e.preventDefault();
    const assignmentItem = data?.find((item) => item.id === id);
    if (assignmentItem) setSelectedItem(assignmentItem);
    setOpen(true);
  };

  const closeDeleteModal = () => {
    setOpenDelModal(false);
    setSelectedItem(null);
  };

  const handleDelete = async () => {
    if (selectedItem) {
      await deleteAssignmentItem(assignmentId, selectedItem.id);
      await mutate(`/assignments/${assignmentId}/items`);
    }
    setSelectedItem(null);
    setOpenDelModal(false);
  };

  const onSubmit = async (itemData: IFormInputs) => {
    const { description, weight } = itemData;
    if (!selectedItem) {
      await addAssignmentItem(assignmentId, {
        description,
        weight,
      });
    } else {
      await updateAssignmentItem(assignmentId, selectedItem.id, {
        description,
        weight,
      });
    }
    await mutate(`/assignments/${assignmentId}/items`);
    setSelectedItem(null);
    reset({ description: '', weight: 1 });
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
    reset({ description: '', weight: 1 });
    setSelectedItem(null);
  };

  useEffect(() => {
    if (selectedItem && open) {
      setValue('description', selectedItem.description);
      setValue('weight', selectedItem.weight);
    }
  }, [open, selectedItem]);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;

  return (
    <Box mt={5}>
      <h2>Evaluation items</h2>
      <Authorization roles={[RolesEnum.developer]}>
        <Box my={4} display="flex" justifyContent="flex-end">
          <Button variant="outlined" color="primary" onClick={() => setOpen(true)}>
            Add new evaluation item
          </Button>
        </Box>
      </Authorization>
      <Grid container spacing={3}>
        {data?.map((item, index) => (
          <Grid item xs={12} md={4} key={item.id}>
            <ListItemCard
              title={item.description}
              circleCharacter={(index + 1).toString()}
              id={item.id}
              module={ModuleEnum.item}
              onEdit={openEditModal}
              onDelete={openDeleteModal}
            />
          </Grid>
        ))}
      </Grid>

      <Authorization roles={[RolesEnum.developer]}>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <DialogContent>
            <h2>{selectedItem ? 'Edit item' : 'New assignment item'}</h2>
            <p>Complete the information below</p>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                control={control}
                as={TextField}
                name="description"
                defaultValue=""
                label="Description"
                placeholder="Description"
                margin="normal"
                error={!!errors.description}
                helperText={errors.description?.message}
                multiline
                rows={2}
                rowsMax={4}
                fullWidth
              />
              <Controller
                control={control}
                as={TextField}
                name="weight"
                defaultValue={1}
                type="number"
                label="Weight"
                placeholder="Weight"
                margin="normal"
                error={!!errors.weight}
                helperText={errors.weight?.message}
                fullWidth
              />
              <DialogActions>
                <Button type="submit" variant="contained" color="primary" fullWidth>
                  {selectedItem ? 'Save item' : 'Add item'}
                </Button>
              </DialogActions>
            </form>
          </DialogContent>
        </Dialog>

        <ConfirmDialog
          open={openDelModal}
          title="Confirm action"
          textContent="Do you want to delete this item? This action cannot be undone"
          handleConfirm={handleDelete}
          handleCancel={closeDeleteModal}
        />
      </Authorization>
    </Box>
  );
};

export default AssignmentItemList;
