import React, { useEffect, useState } from 'react';

import { Box, CircularProgress } from '@material-ui/core';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ExecutionOverview from 'components/Execution';
import ModulesList from 'components/ModulesList';
import ErrorHandler from 'components/RequestErrorHandler';
import IModule from 'interfaces/IModule';
import IProgram from 'interfaces/IProgram';
import { calculateProgramProgress } from 'utils/utils';

interface Props {
  programId: string;
}

const ProgramInfo = ({ programId }: Props): JSX.Element => {
  const { data, error, isValidating } = useSWR<IProgram>(`/programs/${programId}`, fetcher);

  const { data: modules, isValidating: modulesValidating, error: modulesError } = useSWR<IModule[]>(
    `/modules?program=${programId}`,
    fetcher
  );

  const [progress, setProgress] = useState(0);
  const [programState, setProgramState] = useState<'In progress' | 'Completed'>('In progress');

  useEffect(() => {
    if (data) {
      const prog = calculateProgramProgress(data);
      setProgress(prog < 100 ? prog : 100);
      if (prog >= 100) setProgramState('Completed');
    }
  }, [data]);

  if ((error && !isValidating) || (modulesError && !modulesValidating))
    return <ErrorHandler errorCode={error || modulesError} />;

  return (
    <Box mt={8}>
      {!data && <CircularProgress />}
      {data && (
        <ExecutionOverview
          title={data.name}
          description={data.description}
          trainers={data.trainers}
          progress={progress}
          state={programState}
        />
      )}
      {!modules && modulesValidating && <CircularProgress />}
      {!modules && modulesError && (
        <Box display="flex" justifyContent="center" mt={4}>
          <p>Could not load modules</p>
        </Box>
      )}
      {modules && <ModulesList modules={modules} programId={programId} />}
    </Box>
  );
};

export default ProgramInfo;
