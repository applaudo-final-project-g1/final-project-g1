import React, { memo, useCallback } from 'react';

import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { throttle } from 'lodash';
import { Link, useHistory } from 'react-router-dom';

import { logout } from 'api/userService';
import useUser from 'hooks/useUser';
import { RolesEnum } from 'interfaces/IEmployee';
import { IUser } from 'interfaces/IUser';
import { IMenuItem } from 'interfaces/menu';
import Routes from 'navigation/Routes';

import payloadMenu from './menuConfig';
import Styles from './styles.module.scss';

type Props = {
  userRole: RolesEnum;
};

const Menu = ({ userRole }: Props): JSX.Element => {
  const history = useHistory();
  const [, setUser] = useUser();

  const retrievePayloadData = () => {
    switch (userRole) {
      case RolesEnum.trainee:
        return payloadMenu.Trainee;
      case RolesEnum.trainer:
        return payloadMenu.Trainer;
      case RolesEnum.developer:
        return payloadMenu.Developer;
      case RolesEnum.manager:
        return payloadMenu.Manager;
      default:
        throw new Error('Role not found');
    }
  };

  const delayedLogout = useCallback(
    throttle(
      () => {
        logout().then(() => {
          setUser({} as IUser);
          history.push(Routes.LOGIN, { expired: false });
        });
      },
      1000,
      { trailing: false }
    ),
    []
  );

  const handleExitButton = () => {
    delayedLogout();
  };

  const data = retrievePayloadData();
  return (
    <List>
      {data.map(({ name, route, icon: Icon }: IMenuItem) => (
        <Link to={route} key={name}>
          <ListItem button>
            <ListItemIcon className={Styles.icons}>
              <Icon />
            </ListItemIcon>
            <ListItemText primary={name} />
          </ListItem>
        </Link>
      ))}
      <ListItem button onClick={handleExitButton}>
        <ListItemIcon className={Styles.icons}>
          <ExitToAppIcon />
        </ListItemIcon>
        <ListItemText primary="Logout" />
      </ListItem>
    </List>
  );
};

export default memo(Menu);
