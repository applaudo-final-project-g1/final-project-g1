import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import GradeIcon from '@material-ui/icons/Grade';
import HomeIcon from '@material-ui/icons/Home';

import Routes from 'navigation/Routes';

const traineeMenu = [
  { name: 'Home', icon: HomeIcon, route: Routes.HOME },
  { name: 'Grades', icon: GradeIcon, route: Routes.GRADES },
  { name: 'Profile', icon: AccountCircleIcon, route: Routes.PROFILE },
];

const trainerMenu = [
  { name: 'Home', icon: HomeIcon, route: Routes.HOME },
  { name: 'Profile', icon: AccountCircleIcon, route: Routes.PROFILE },
];

const developerMenu = [
  { name: 'Home', icon: HomeIcon, route: Routes.HOME },
  { name: 'Profile', icon: AccountCircleIcon, route: Routes.PROFILE },
];

const managerMenu = [
  { name: 'Home', icon: HomeIcon, route: Routes.HOME },
  { name: 'Profile', icon: AccountCircleIcon, route: Routes.PROFILE },
];

const payloadMenu = {
  Trainee: traineeMenu,
  Trainer: trainerMenu,
  Developer: developerMenu,
  Manager: managerMenu,
};

export default payloadMenu;
