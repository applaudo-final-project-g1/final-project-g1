import { Actions, ModulesListAction, State } from './actions';

const ModulesListReducer = (state: State, action: ModulesListAction): State => {
  switch (action.type) {
    case Actions.SET_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case Actions.SET_SUCCESS:
      return {
        ...state,
        selectedModule: null,
        openFormModal: false,
        openDeleteModal: false,
        isLoading: false,
        error: '',
      };

    case Actions.SET_ERROR:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };

    case Actions.NEW_MODULE_FORM:
      return {
        ...state,
        selectedModule: null,
        openFormModal: true,
      };

    case Actions.EDIT_MODULE_FORM:
      return {
        ...state,
        selectedModule: action.payload.module,
        openFormModal: true,
      };

    case Actions.CLOSE_MODULE_FORM:
      return {
        ...state,
        selectedModule: null,
        openFormModal: false,
        error: '',
      };

    case Actions.OPEN_DELETE_FORM:
      return {
        ...state,
        selectedModule: action.payload.module,
        openDeleteModal: true,
      };

    case Actions.CLOSE_DELETE_FORM:
      return {
        ...state,
        openDeleteModal: false,
      };

    default:
      return state;
  }
};

export default ModulesListReducer;
