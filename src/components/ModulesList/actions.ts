import IModule from 'interfaces/IModule';

export enum Actions {
  NEW_MODULE_FORM = 'NEW_MODULE',
  EDIT_MODULE_FORM = 'EDIT_MODULE_FORM',
  CLOSE_MODULE_FORM = 'CLOSE_MODULE_FORM',
  OPEN_DELETE_FORM = 'OPEN_DELETE_FORM',
  CLOSE_DELETE_FORM = ' CLOSE_DELETE_FORM',
  SET_LOADING = 'SET_LOADING',
  SET_SUCCESS = 'SET_SUCCESS',
  SET_ERROR = 'SET_ERROR',
}

export type State = {
  openFormModal: boolean;
  openDeleteModal: boolean;
  selectedModule?: IModule | null;
  isLoading: boolean;
  error: string;
};

export type ModulesListAction =
  | { type: Actions.NEW_MODULE_FORM }
  | { type: Actions.EDIT_MODULE_FORM; payload: { module: IModule } }
  | { type: Actions.CLOSE_MODULE_FORM }
  | { type: Actions.OPEN_DELETE_FORM; payload: { module: IModule } }
  | { type: Actions.CLOSE_DELETE_FORM }
  | { type: Actions.SET_LOADING }
  | { type: Actions.SET_SUCCESS }
  | { type: Actions.SET_ERROR; payload: { error: string } };
