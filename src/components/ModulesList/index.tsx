import React, { useEffect, useReducer } from 'react';

import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Button, CircularProgress, Grid, Modal, TextField } from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { mutate } from 'swr';
import * as yup from 'yup';

import add, { deleteModule, edit } from 'api/moduleService';
import Authorization from 'components/Authorization';
import ConfirmDialog from 'components/ConfirmDialog';
import ListItemCard from 'components/ListItemCard';
import { RolesEnum } from 'interfaces/IEmployee';
import IModule from 'interfaces/IModule';
import { ModuleEnum } from 'navigation/rbacRules';

import { Actions, State } from './actions';
import ModulesListReducer from './reducer';
import styles from './styles.module.scss';

interface Props {
  modules: IModule[];
  programId: string;
}

interface IFormInputs {
  name: string;
  description: string;
}

const schema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

const initialState: State = {
  isLoading: false,
  error: '',
  selectedModule: null,
  openFormModal: false,
  openDeleteModal: false,
};

const ModulesList = ({ modules, programId }: Props): JSX.Element => {
  const [state, dispatch] = useReducer(ModulesListReducer, initialState);

  const { handleSubmit, control, errors, setValue, reset } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
    shouldUnregister: false,
  });

  const handleOpen = () => {
    dispatch({ type: Actions.NEW_MODULE_FORM });
  };

  const handleClose = () => {
    dispatch({ type: Actions.CLOSE_MODULE_FORM });
    reset({ name: '', description: '' });
  };

  const handleCloseDelModal = () => {
    dispatch({ type: Actions.CLOSE_DELETE_FORM });
  };

  const handleEdit = (e: React.MouseEvent<HTMLButtonElement>, id: number) => {
    e.preventDefault();
    const module = modules.find((m) => m.id === id)!;
    dispatch({ type: Actions.EDIT_MODULE_FORM, payload: { module } });
  };

  const openDeleteModal = (e: React.MouseEvent<HTMLButtonElement>, id: number) => {
    e.preventDefault();
    const module = modules.find((m) => m.id === id)!;
    dispatch({ type: Actions.OPEN_DELETE_FORM, payload: { module } });
  };

  const onDelete = async () => {
    try {
      dispatch({ type: Actions.SET_LOADING });
      if (state.selectedModule) {
        await deleteModule(state.selectedModule.id);
        await mutate(`/modules?program=${programId}`);
      }
      dispatch({ type: Actions.SET_SUCCESS });
    } catch {
      dispatch({ type: Actions.SET_ERROR, payload: { error: 'Could not delete this module 😥' } });
    }
  };

  const onSubmit = async (data: IFormInputs) => {
    const { selectedModule } = state;
    try {
      dispatch({ type: Actions.SET_LOADING });
      const { description, name } = data;
      if (!selectedModule) {
        await add({
          description,
          name,
          programId,
        });
      } else {
        await edit(selectedModule.id, {
          description,
          name,
        });
      }
      await mutate(`/modules?program=${programId}`);
      reset({ name: '', description: '' });
      dispatch({ type: Actions.SET_SUCCESS });
    } catch {
      dispatch({
        type: Actions.SET_ERROR,
        payload: { error: `Could not ${selectedModule ? 'edit' : add} module 😥` },
      });
    }
  };

  useEffect(() => {
    const { selectedModule, openFormModal } = state;
    if (selectedModule && openFormModal) {
      setValue('name', selectedModule.name);
      setValue('description', selectedModule.description);
    }
  }, [state.openFormModal, state.selectedModule]);

  const { isLoading, error, openFormModal, openDeleteModal: openDelModal, selectedModule } = state;
  return (
    <Box px={2} mt={5}>
      <h2>Modules</h2>
      <Grid container spacing={3}>
        {modules.map((module, index) => (
          <Grid item xs={12} md={4} key={module.id}>
            <Link to={`/modules/${module.id}`} style={{ textDecoration: 'none' }}>
              <ListItemCard
                title={module.name}
                id={module.id}
                module={ModuleEnum.module}
                onEdit={handleEdit}
                onDelete={openDeleteModal}
                circleCharacter={(index + 1).toString()}
              />
            </Link>
          </Grid>
        ))}
      </Grid>
      {modules.length === 0 && <p>No modules found</p>}
      <Authorization roles={[RolesEnum.developer]}>
        <Box m={2} />
        <Button type="button" onClick={handleOpen} variant="contained" color="primary">
          New module
        </Button>
        <Box m={2} />
        <Modal
          open={openFormModal}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div className={styles.modal}>
            <h2 id="simple-modal-title">{selectedModule ? 'Edit module' : 'New module'}</h2>
            <p id="simple-modal-description">
              Complete the information below to{' '}
              {selectedModule ? 'edit the module' : 'add a new module'}
            </p>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                control={control}
                as={TextField}
                name="name"
                defaultValue=""
                label="Name"
                placeholder="name"
                margin="normal"
                error={!!errors.name}
                helperText={errors.name?.message}
                fullWidth
              />
              <Controller
                control={control}
                as={TextField}
                name="description"
                defaultValue=""
                label="Description"
                placeholder="description"
                margin="normal"
                error={!!errors.description}
                helperText={errors.description?.message}
                multiline
                rows={2}
                rowsMax={4}
                fullWidth
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disabled={isLoading}
                fullWidth
              >
                {selectedModule ? 'Edit module' : 'Add module'}
              </Button>
              <div className={styles.modalMessages}>
                {isLoading && <CircularProgress color="primary" />}
                {error && <p>{error}</p>}
              </div>
            </form>
          </div>
        </Modal>

        <ConfirmDialog
          open={openDelModal}
          title="Confirm action"
          textContent="Do you want to delete this module? This action cannot be undone."
          handleConfirm={onDelete}
          handleCancel={handleCloseDelModal}
        />
      </Authorization>
    </Box>
  );
};

export default ModulesList;
