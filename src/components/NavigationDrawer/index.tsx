import React, { useState } from 'react';

import {
  AppBar,
  Hidden,
  IconButton,
  Toolbar,
  Drawer,
  Divider,
  makeStyles,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';

import NavyLogo from 'assets/images/NavyLogo.png';
import Menu from 'components/Menu';
import useUser from 'hooks/useUser';
import Routes from 'navigation/Routes';
import precedenceRole from 'utils/precedenceRole';

import Styles from './styles.module.scss';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
}));

const NavigationDrawer = (): JSX.Element => {
  const [mobileOpen, setMobileOpen] = useState<boolean>(false);
  const [user] = useUser();
  const classes = useStyles();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <Link to={Routes.HOME}>
        <div className={Styles.headerDrawer}>
          <img src={NavyLogo} alt="Logo" className={Styles.logoImg} />
        </div>
      </Link>
      <Divider />
      <Menu userRole={precedenceRole(user.roles)} />
      <div style={{ position: 'absolute', bottom: 0, width: '100%' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <p className={Styles.copyMessage}>Applaudo Studios &copy; {new Date().getFullYear()}</p>
        </div>
      </div>
    </div>
  );

  return (
    <>
      <Hidden smUp>
        <AppBar className={Styles.appBar} position="fixed">
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </Hidden>
      <nav className={classes.drawer}>
        <Hidden smUp>
          <Drawer
            container={window.document.body}
            variant="temporary"
            open={mobileOpen}
            anchor="left"
            onClose={handleDrawerToggle}
            ModalProps={{ keepMounted: true }}
            classes={{ paper: Styles.drawerMobile }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown>
          <Drawer open variant="permanent" classes={{ paper: Styles.drawer }}>
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </>
  );
};

export default NavigationDrawer;
