import { makeStyles } from '@material-ui/core';

const useClickableTableStyles = makeStyles(() => ({
  clickableRow: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  cell: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

export default useClickableTableStyles;
