import React from 'react';

import { TableRow, TableCell } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import { ITableRow } from 'interfaces/generics';

import useClickableTableStyles from './clickableTableStyles';

type Props<T> = {
  items: ITableRow<T>[];
  pathTo: string;
};

const ClickableTable = <T,>({ items, pathTo }: Props<T>): JSX.Element => {
  const history = useHistory();
  const classes = useClickableTableStyles();

  const handleClick = (payload: T, isClickable: boolean) => {
    if (isClickable) history.push(pathTo, { payload });
  };

  return (
    <>
      {items.map((row) => (
        <TableRow
          key={row.id}
          classes={{ root: row.clickable ? classes.clickableRow : '' }}
          onClick={() => handleClick(row.payload, row.clickable)}
        >
          {Object.entries(row).map((info) => {
            if (info[0] !== 'id' && info[0] !== 'payload' && info[0] !== 'clickable') {
              return (
                <TableCell key={info[1]} align="center">
                  <div className={classes.cell}>{info[1]}</div>
                </TableCell>
              );
            }
            return null;
          })}
        </TableRow>
      ))}
    </>
  );
};

export default ClickableTable;
