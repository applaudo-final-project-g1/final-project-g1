import React from 'react';

import { CircularProgress } from '@material-ui/core';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ProfileInfo from 'components/ProfileInfo';
import ErrorHandler from 'components/RequestErrorHandler';
import useUser from 'hooks/useUser';
import IEmployee from 'interfaces/IEmployee';

const WithEmployeeProfile = (): JSX.Element => {
  const [user] = useUser();
  const { data, error, isValidating } = useSWR<IEmployee>(`/employees/${user.userId}`, fetcher);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;
  if (!data) return <CircularProgress color="primary" />;

  return <ProfileInfo info={data} />;
};

export default WithEmployeeProfile;
