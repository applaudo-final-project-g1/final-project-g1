import React from 'react';

import { CircularProgress } from '@material-ui/core';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ProfileInfo from 'components/ProfileInfo';
import ErrorHandler from 'components/RequestErrorHandler';
import IEmployee from 'interfaces/IEmployee';

const WithTraineeProfile = (): JSX.Element => {
  const { data, error, isValidating } = useSWR<IEmployee>('/trainees/me', fetcher);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;
  if (!data) return <CircularProgress color="primary" />;

  return <ProfileInfo info={data} />;
};

export default WithTraineeProfile;
