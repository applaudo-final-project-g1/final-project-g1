/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';

export interface TabItem {
  id: number;
  label: string;
  children: React.ReactNode;
}

interface TabViewProps {
  tabs: TabItem[];
}

interface TabPanelProps {
  children?: React.ReactNode;
  value: number;
  index: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
    >
      {value === index && children}
    </div>
  );
}

function tabProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
  },
}));

export default function TabView({ tabs }: TabViewProps): JSX.Element {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<Record<string, unknown>>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default" elevation={1}>
        <Tabs value={value} onChange={handleChange} aria-label="Tabs panel">
          {tabs.map((tab, index) => (
            <Tab key={tab.id} label={tab.label} {...tabProps(index)} />
          ))}
        </Tabs>
      </AppBar>
      {tabs.map((tab, index) => (
        <TabPanel key={tab.id} value={value} index={index}>
          {tab.children}
        </TabPanel>
      ))}
    </div>
  );
}

TabPanel.defaultProps = {
  children: null,
};
