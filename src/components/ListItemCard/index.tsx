/* eslint-disable no-console */
import React from 'react';

import { Avatar, Card, CardHeader, IconButton, LinearProgress } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import AuthorizedTo from 'components/AuthorizedTo';
import { ModuleEnum, PermissionEnum } from 'navigation/rbacRules';

import styles from './styles.module.scss';

interface Props {
  title?: string;
  subtitle?: string;
  circleCharacter?: string;
  progress?: number;
  id?: number;
  module: ModuleEnum;
  onEdit?(e: React.MouseEvent<HTMLButtonElement>, id: number): void;
  onDelete?(e: React.MouseEvent<HTMLButtonElement>, id: number): void;
}

const ListItemCard = ({
  title,
  subtitle,
  circleCharacter,
  progress,
  id,
  module,
  onEdit,
  onDelete,
}: Props): JSX.Element => {
  return (
    <Card>
      <CardHeader
        avatar={
          <Avatar aria-label={title} className={styles.avatarIcon}>
            {circleCharacter}
          </Avatar>
        }
        title={title}
        subheader={subtitle}
        action={
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            {onEdit && id && (
              <AuthorizedTo
                module={module}
                permission={PermissionEnum.edit}
                yes={
                  <IconButton
                    color="inherit"
                    aria-label="exit button"
                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => onEdit(e, id)}
                  >
                    <EditIcon />
                  </IconButton>
                }
              />
            )}
            {onDelete && id && (
              <AuthorizedTo
                module={module}
                permission={PermissionEnum.delete}
                yes={
                  <IconButton
                    color="inherit"
                    aria-label="exit button"
                    onClick={(e: React.MouseEvent<HTMLButtonElement>) => onDelete(e, id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                }
              />
            )}
          </div>
        }
      />
      {progress && <LinearProgress variant="determinate" value={60} />}
    </Card>
  );
};

ListItemCard.defaultProps = {
  title: null,
  subtitle: null,
  circleCharacter: null,
  progress: null,
  id: null,
  onEdit: null,
  onDelete: null,
};

export default ListItemCard;
