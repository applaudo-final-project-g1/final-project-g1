import { makeStyles } from '@material-ui/core';

const useDisplayTableStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: theme.palette.secondary.main,
  },
  cell: {
    color: 'white',
    fontWeight: 'bold',
    whiteSpace: 'nowrap',
  },
  table: {
    tableLayout: 'fixed',
  },
}));

export default useDisplayTableStyles;
