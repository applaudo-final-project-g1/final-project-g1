import React, { memo, ReactNode } from 'react';

import {
  TableContainer,
  Paper,
  TableHead,
  Table,
  TableRow,
  TableBody,
  TableCell,
} from '@material-ui/core';

import useDisplayTableStyles from './displayTableStyles';

type Props = {
  headers: string[];
  ariaText: string;
  children: ReactNode;
};

const WrapperTable = ({ headers, ariaText, children }: Props): JSX.Element => {
  const classes = useDisplayTableStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label={ariaText}>
        <TableHead className={classes.header}>
          <TableRow>
            {headers.map((header) => (
              <TableCell key={header} className={classes.cell} align="center">
                {header}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>{children}</TableBody>
      </Table>
    </TableContainer>
  );
};

export default memo(WrapperTable);
