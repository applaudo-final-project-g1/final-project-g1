import React, { memo, ReactNode } from 'react';

import NavigationDrawer from 'components/NavigationDrawer';

import useDrawerLayoutStyles from './drawerLayoutStyles';

type Props = {
  children: ReactNode;
};

const DrawerLayout = ({ children }: Props): JSX.Element => {
  const classes = useDrawerLayoutStyles();

  return (
    <div className={classes.root}>
      <NavigationDrawer />
      <main className={classes.content}>{children}</main>
    </div>
  );
};

export default memo(DrawerLayout);
