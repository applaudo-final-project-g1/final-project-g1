import { makeStyles } from '@material-ui/core';

const useDrawerLayoutStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(2),
  },
}));

export default useDrawerLayoutStyles;
