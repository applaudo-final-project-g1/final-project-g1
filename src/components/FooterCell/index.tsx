import React, { ReactNode } from 'react';

import { TableCell } from '@material-ui/core';

import useFooterCellStyles from './footerCellStyles';

type Props = {
  children: ReactNode;
};

const FooterCell = ({ children }: Props): JSX.Element => {
  const classes = useFooterCellStyles();

  return (
    <TableCell classes={{ root: classes.content }} align="center">
      {children}
    </TableCell>
  );
};

export default FooterCell;
