import { makeStyles } from '@material-ui/core';

const useFooterCellStyles = makeStyles(() => ({
  content: {
    fontWeight: 'bold',
  },
}));

export default useFooterCellStyles;
