/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';

import { yupResolver } from '@hookform/resolvers/yup';
import {
  Box,
  Button,
  CircularProgress,
  FormControlLabel,
  Switch,
  TextField,
} from '@material-ui/core';
import { Controller, ControllerRenderProps, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import useSWR from 'swr';
import * as yup from 'yup';

import { addAssignment, updateAssignment } from 'api/assignmentService';
import fetcher from 'api/axiosFetcher';
import { IAssignment } from 'interfaces/IAssignment';

const schema = yup.object().shape({
  label: yup.string().required(),
  weight: yup.number().positive().required(),
  canSubmit: yup.boolean(),
});

interface Props {
  assignmentId?: string;
  moduleId: string;
}

interface FormInputs {
  label: string;
  weight: number;
  canSubmit: boolean;
}

const AssignmentForm = ({ moduleId, assignmentId }: Props): JSX.Element => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const { data } = useSWR<IAssignment>(
    assignmentId ? `assignments/${assignmentId}` : null,
    fetcher
  );

  const { handleSubmit, control, setValue, errors } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    if (data) {
      const { label, weight, canSubmit } = data;
      setValue('label', label);
      setValue('weight', weight);
      setValue('canSubmit', canSubmit);
    }
  }, [data]);

  const onSubmit = async (assignmentData: FormInputs) => {
    try {
      setLoading(true);
      setError('');
      const { label, weight, canSubmit } = assignmentData;
      if (!assignmentId) {
        await addAssignment({
          label,
          weight,
          canSubmit,
          moduleId,
        });
      } else {
        await updateAssignment(assignmentId.toString(), {
          label,
          weight,
          canSubmit,
        });
      }
      setLoading(false);
      history.goBack();
    } catch {
      setError('Could not save assignment');
      setLoading(false);
    }
  };

  return (
    <Box mt={4}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          as={TextField}
          name="label"
          defaultValue=""
          label="Label"
          placeholder="Label"
          margin="normal"
          error={!!errors.label}
          helperText={errors.label?.message}
          fullWidth
        />
        <Controller
          control={control}
          as={TextField}
          name="weight"
          defaultValue={1}
          type="number"
          label="Weight"
          placeholder="Weight"
          margin="normal"
          error={!!errors.weight}
          helperText={errors.weight?.message}
          fullWidth
        />
        <Box my={3}>
          <Controller
            control={control}
            name="canSubmit"
            render={(props: ControllerRenderProps<Record<string, boolean>>) => (
              <FormControlLabel
                control={
                  <Switch
                    color="primary"
                    onChange={(e) => props.onChange(e.target.checked)}
                    checked={props.value}
                  />
                }
                label="Can submit?"
              />
            )}
            defaultValue={false}
          />
        </Box>

        <Button type="submit" variant="contained" color="primary" disabled={loading}>
          Save assignment
        </Button>

        <section>
          {error && <p>{error}</p>}
          {loading && <CircularProgress color="primary" />}
        </section>
      </form>
    </Box>
  );
};

AssignmentForm.defaultProps = {
  assignmentId: null,
};

export default AssignmentForm;
