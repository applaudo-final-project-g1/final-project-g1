import { RolesEnum } from 'interfaces/IEmployee';

const precedenceRole = (roles: RolesEnum[]): RolesEnum => {
  if (roles.includes(RolesEnum.developer)) {
    return RolesEnum.developer;
  }
  if (roles.includes(RolesEnum.trainer)) {
    return RolesEnum.trainer;
  }
  return RolesEnum.trainee;
};

export default precedenceRole;
