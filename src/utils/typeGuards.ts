import { GoogleLoginResponse } from 'react-google-login';

import ITrainee from 'interfaces/ITrainee';

const isITraineeObject = (obj: unknown): obj is ITrainee => {
  return (obj as ITrainee).programId !== undefined;
};

const isGoogleLoginResponse = (obj: unknown): obj is GoogleLoginResponse => {
  return (obj as GoogleLoginResponse).tokenId !== undefined;
};

export { isITraineeObject, isGoogleLoginResponse };
