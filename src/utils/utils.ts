import IProgram, { IProgramWithModules } from 'interfaces/IProgram';

const calculateItemGrade = (weight: number, percentage: string): number => {
  return weight * (parseFloat(percentage) / 100);
};

const convertGradeToBaseTen = (basedGrade: number, base: number): number => {
  return parseFloat(((basedGrade * 10) / base).toFixed(2));
};

const dateDiffInDays = (d1: Date, d2: Date): number => {
  const MS_PER_DAY = 1000 * 60 * 60 * 24;
  // Discard the time and time-zone information.
  const utc1 = Date.UTC(d1.getFullYear(), d1.getMonth(), d1.getDate());
  const utc2 = Date.UTC(d2.getFullYear(), d2.getMonth(), d2.getDate());

  return Math.floor((utc2 - utc1) / MS_PER_DAY);
};

const progressByTotalPassedDate = (totalDays: number, passedDays: number): number => {
  return Math.floor((passedDays * 100) / totalDays);
};

const calculateProgramProgress = (program: IProgram | IProgramWithModules): number => {
  const startDate = new Date(program.startDate);
  const endDate = new Date(program.endDate);
  const today = new Date();
  const totalDays = dateDiffInDays(startDate, endDate);
  const passedDays = dateDiffInDays(today, endDate);
  const progress = progressByTotalPassedDate(totalDays, passedDays);
  return progress;
};

export {
  calculateItemGrade,
  convertGradeToBaseTen,
  dateDiffInDays,
  progressByTotalPassedDate,
  calculateProgramProgress,
};
