import React from 'react';

import { Container, CssBaseline, Box, Button } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import AssignmentForm from 'components/AssignmentForm';
import AssignmentItemList from 'components/AssignmentItemList';
import AssignmentTrainer from 'components/AssignmentTrainer';
import ErrorHandler from 'components/RequestErrorHandler';
import TabView, { TabItem } from 'components/TabView';
import useUser from 'hooks/useUser';
import { RolesEnum } from 'interfaces/IEmployee';

import useAssignmentDetailStyles from './assignmentDetailStyles';

interface PathParams {
  moduleId: string;
  assignmentId: string;
}

const AssignmentDetail = (): JSX.Element => {
  const { moduleId, assignmentId } = useParams<PathParams>();
  const history = useHistory();
  const [user] = useUser();
  const classes = useAssignmentDetailStyles();

  const { error, isValidating } = useSWR(`/modules/${moduleId}`, fetcher);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;

  const tabsList = () => {
    const tabs: TabItem[] = [];

    if (user.roles.includes(RolesEnum.trainer))
      tabs.push({
        id: 1,
        label: 'Detail',
        children: <AssignmentTrainer assignmentId={assignmentId} />,
      });

    if (user.roles.includes(RolesEnum.developer))
      tabs.push({
        id: 2,
        label: 'Edit',
        children: (
          <>
            <AssignmentForm assignmentId={assignmentId} moduleId={moduleId} />
            <Box my={3}>
              <AssignmentItemList assignmentId={assignmentId} />
            </Box>
          </>
        ),
      });

    return tabs;
  };

  return (
    <Container className={classes.root} maxWidth="lg">
      <CssBaseline />
      <Box m={2} mt={8}>
        <h1>Assignment</h1>
        <Box m={2} display="flex" justifyContent="space-between">
          <Button onClick={() => history.goBack()}>Go back</Button>
        </Box>

        {assignmentId ? (
          <TabView tabs={tabsList()} />
        ) : (
          <AssignmentForm assignmentId={assignmentId} moduleId={moduleId} />
        )}
      </Box>
    </Container>
  );
};

export default AssignmentDetail;
