import { makeStyles } from '@material-ui/core';

const useAssignmentDetailStyles = makeStyles(() => ({
  root: {
    paddingLeft: '0px',
    paddingRight: '0px',
  },
}));

export default useAssignmentDetailStyles;
