import React, { useState } from 'react';

import { Container, CssBaseline, Box, Grid, Button, CircularProgress } from '@material-ui/core';
import { useHistory, useParams, Link } from 'react-router-dom';
import useSWR, { mutate } from 'swr';

import { deleteAssigment } from 'api/assignmentService';
import fetcher from 'api/axiosFetcher';
import Authorization from 'components/Authorization';
import ConfirmDialog from 'components/ConfirmDialog';
import ListItemCard from 'components/ListItemCard';
import ErrorHandler from 'components/RequestErrorHandler';
import { IAssignment } from 'interfaces/IAssignment';
import { RolesEnum } from 'interfaces/IEmployee';
import IModule from 'interfaces/IModule';
import { ModuleEnum } from 'navigation/rbacRules';

interface PathParams {
  moduleId: string;
}

const ModuleDetail = (): JSX.Element => {
  const { moduleId } = useParams<PathParams>();
  const history = useHistory();

  const [open, setOpen] = useState(false);
  const [selectedAssignment, setSelectedAssignment] = useState(0);

  const { data: module } = useSWR<IModule>(`/modules/${moduleId}`, fetcher);
  const { data, error, isValidating } = useSWR<IAssignment[]>(
    `/assignments?moduleId=${moduleId}`,
    fetcher
  );

  const handleDeleteModal = (e: React.MouseEvent<HTMLButtonElement>, id: number) => {
    e.preventDefault();
    setSelectedAssignment(id);
    setOpen(true);
  };

  const confirmDelete = async () => {
    if (selectedAssignment) {
      await deleteAssigment(selectedAssignment);
      await mutate(`/assignments?moduleId=${moduleId}`);
    }
    setSelectedAssignment(0);
    setOpen(false);
  };

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <Box m={2} mt={8}>
        {module ? <h1>{module.name}</h1> : <CircularProgress color="primary" />}

        <Box m={2} display="flex" justifyContent="space-between">
          <Button onClick={() => history.goBack()}>Go back</Button>
          <Authorization roles={[RolesEnum.developer]}>
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to={`/modules/${moduleId}/assignment`}
            >
              Create new assignment
            </Button>
          </Authorization>
        </Box>
        <Grid container spacing={3}>
          {data?.map((assignment, index) => (
            <Grid item xs={12} key={assignment.id}>
              <Link
                to={`/modules/${moduleId}/assignment/${assignment.id}`}
                style={{ textDecoration: 'none' }}
              >
                <ListItemCard
                  title={assignment.label}
                  subtitle={`Weight : ${assignment.weight}`}
                  circleCharacter={(index + 1).toString()}
                  id={assignment.id}
                  module={ModuleEnum.assignment}
                  onDelete={handleDeleteModal}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Box>
      <ConfirmDialog
        open={open}
        title="Confirm action"
        textContent="Do you really want to delete this assignment? This action cannot be undone"
        handleConfirm={confirmDelete}
        handleCancel={() => setOpen(false)}
      />
    </Container>
  );
};

export default ModuleDetail;
