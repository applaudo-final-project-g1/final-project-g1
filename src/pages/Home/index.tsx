import React from 'react';

import { Container, CssBaseline } from '@material-ui/core';

import Authorization from 'components/Authorization';
import ProgramsList from 'components/ProgramsList';
import TraineeHome from 'components/TraineeHome';
import { RolesEnum } from 'interfaces/IEmployee';

const Home = (): JSX.Element => {
  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <Authorization roles={[RolesEnum.trainee]}>
        <TraineeHome />
      </Authorization>

      <Authorization roles={[RolesEnum.developer, RolesEnum.manager, RolesEnum.trainer]}>
        <ProgramsList />
      </Authorization>
    </Container>
  );
};

export default Home;
