import React from 'react';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  CssBaseline,
  Grid,
  Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Redirect, useLocation, useHistory } from 'react-router-dom';

import { ITraineeGrades } from 'interfaces/IGrade';
import Routes from 'navigation/Routes';

const GradesDetails = (): JSX.Element => {
  const location = useLocation<{ payload: ITraineeGrades }>();

  const history = useHistory();

  const module = location.state.payload;

  if (!module) return <Redirect to={Routes.HOME} />;

  const findItemFeedBack = (assignmentIndex: number, itemId: number) => {
    return module.assignments[assignmentIndex].evaluations[0].grades.find(
      (grade) => itemId === grade.assignmentItemId
    );
  };

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <h1 className="verticalMargin">{module.name}</h1>
      <div className="verticalMargin">
        <div className="verticalMargin">
          <Button onClick={() => history.goBack()}>Go back</Button>
        </div>
        {module.assignments.map((assingment, index) => (
          <Accordion key={assingment.id}>
            <AccordionSummary
              aria-controls="panel1a-content"
              id="panel1a-header"
              expandIcon={<ExpandMoreIcon />}
            >
              <Box display="flex" justifyContent="space-between" width="100%">
                <h2>{assingment.label}</h2>
                <h2>Grade: {assingment.evaluations[0].grade?.toFixed(2)}</h2>
              </Box>
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={3}>
                {assingment.assignmentItems.map((item) => (
                  <Grid item xs={12} md={4} key={item.id}>
                    <Card className="fullHeight">
                      <CardHeader subheader={`${item.description} (${item.weight} %)`} />
                      <CardContent>
                        <Typography variant="h6" color="textPrimary" gutterBottom>
                          {findItemFeedBack(index, item.id)?.scale?.label}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" gutterBottom>
                          {findItemFeedBack(index, item.id)?.feedback || 'No comments'}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </AccordionDetails>
          </Accordion>
        ))}
      </div>
    </Container>
  );
};

export default GradesDetails;
