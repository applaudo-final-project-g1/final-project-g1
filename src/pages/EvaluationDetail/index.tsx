import React from 'react';

import { Container, CssBaseline } from '@material-ui/core';
import { Redirect, useLocation } from 'react-router-dom';

import Authorization from 'components/Authorization';
import Evaluate from 'components/Evaluate';
import { RolesEnum } from 'interfaces/IEmployee';
import { IEvaluation } from 'interfaces/IEvaluation';
import Routes from 'navigation/Routes';

const EvaluationDetail = (): JSX.Element => {
  const location = useLocation<{ payload: IEvaluation }>();

  const evaluation = location.state.payload;

  if (!evaluation) return <Redirect to={Routes.HOME} />;

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <Authorization roles={[RolesEnum.trainer]}>
        <Evaluate evaluation={evaluation} />
      </Authorization>
    </Container>
  );
};

export default EvaluationDetail;
