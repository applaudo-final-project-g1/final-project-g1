import React from 'react';

import { Box, Container, CssBaseline } from '@material-ui/core';

import Authorization from 'components/Authorization';
import WithEmployeeProfile from 'components/WithEmployeeProfile';
import WithTraineeProfile from 'components/WithTraineeProfile';
import { RolesEnum } from 'interfaces/IEmployee';

const Profile = (): JSX.Element => {
  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <Box mt={8} width="100%">
        <h1>My Profile</h1>
        <Authorization roles={[RolesEnum.trainee]}>
          <WithTraineeProfile />
        </Authorization>
        <Authorization roles={[RolesEnum.developer, RolesEnum.trainer, RolesEnum.manager]}>
          <WithEmployeeProfile />
        </Authorization>
      </Box>
    </Container>
  );
};

export default Profile;
