import React from 'react';

import { Link } from 'react-router-dom';

import NavyLogo from 'assets/images/NavyLogo.png';
import Routes from 'navigation/Routes';

import Styles from './styles.module.scss';

const index = (): JSX.Element => {
  return (
    <div className={Styles.notFoundContainer}>
      <img src={NavyLogo} alt="Logo" className={Styles.logoImg} />
      <h1>Oops!</h1>
      <p>Seems that the page you are looking for does not exist...</p>
      <Link to={Routes.HOME}>
        <p>Go home</p>
      </Link>
    </div>
  );
};

export default index;
