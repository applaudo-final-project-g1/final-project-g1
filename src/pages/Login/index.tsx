/* eslint-disable import/no-extraneous-dependencies */
import React, { useState } from 'react';

import { Container, CircularProgress } from '@material-ui/core';
import jwt_decode from 'jwt-decode';
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login';
import { StaticContext } from 'react-router';
import { RouteComponentProps, useHistory } from 'react-router-dom';

import { login } from 'api/userService';
import Logo from 'assets/images/logo.png';
import useUser from 'hooks/useUser';
import { IUser } from 'interfaces/IUser';
import Routes from 'navigation/Routes';
import { isGoogleLoginResponse } from 'utils/typeGuards';

import styles from './styles.module.scss';

const { REACT_APP_GOOGLE_ID: clientId } = process.env;

type LocationPropsType = { expired?: string };

const Login = ({
  location,
}: RouteComponentProps<{ foo: string }, StaticContext, LocationPropsType>): JSX.Element => {
  const history = useHistory();

  const [, setUser] = useUser();

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const onSuccess = (response: GoogleLoginResponse | GoogleLoginResponseOffline): void => {
    setLoading(true);
    if (isGoogleLoginResponse(response))
      login({ googleToken: response.tokenId })
        .then((res) => {
          if (res.status === 200) {
            const { accessToken } = res.data;
            return { employee: jwt_decode<IUser>(accessToken), jwt: accessToken };
          }
          throw new Error(res.status.toString());
        })
        .then(({ employee, jwt }) => {
          setLoading(false);
          setUser({ ...employee, jwt });
          history.push(Routes.HOME);
        })
        .catch((err) => {
          setLoading(false);
          if (err instanceof Error && err.stack?.includes('401')) {
            setError('Invalid credentials 😥');
          }
        });
  };

  const onFailure = () => {
    setError('Invalid credentials 😥');
  };

  return (
    <div className={styles.container}>
      <Container component="main" maxWidth="xs">
        <div className={styles.form}>
          <img src={Logo} alt="Logo" />
          <h1>Campus</h1>
          <p>Applaudo Studios Trainee Program</p>
          <GoogleLogin
            clientId={clientId ?? ''}
            buttonText="Sign in with Google"
            onSuccess={onSuccess}
            onFailure={onFailure}
          />
          {loading && <CircularProgress color="primary" />}
          {error && <p>{error}</p>}
          {location.state.expired && <p>Your session has expired!</p>}
        </div>
      </Container>
    </div>
  );
};

export default Login;
