import React from 'react';

import { Box, Button, Container, CssBaseline } from '@material-ui/core';
import { useHistory, useParams } from 'react-router-dom';

import ProgramInfo from 'components/ProgramInfo';
import Routes from 'navigation/Routes';

const ProgramDetail = (): JSX.Element => {
  interface PathParams {
    programId: string;
  }

  const history = useHistory();
  const { programId } = useParams<PathParams>();

  const handleBack = () => {
    if (history.length <= 2) history.push(Routes.HOME);
    else history.goBack();
  };

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <ProgramInfo programId={programId} />
      <Box m={2} mt={4}>
        <Button onClick={handleBack}>Back</Button>
      </Box>
    </Container>
  );
};

export default ProgramDetail;
