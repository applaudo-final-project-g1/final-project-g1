import React from 'react';

import { Button, CircularProgress, Container, CssBaseline } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import useSWR from 'swr';

import fetcher from 'api/axiosFetcher';
import ClickableTable from 'components/ClickableTable';
import ErrorHandler from 'components/RequestErrorHandler';
import WrapperTable from 'components/WrapperTable';
import { ITraineeGrades } from 'interfaces/IGrade';

const tableHeaders = ['Module', 'Grade'];

const Grades = (): JSX.Element => {
  const history = useHistory();
  const { data, error, isValidating } = useSWR<ITraineeGrades[]>('/trainees/me/grades', fetcher);

  if (error && !isValidating) return <ErrorHandler errorCode={error.message} />;
  if (!data) return <CircularProgress color="primary" />;

  const calculateGrade = (module: ITraineeGrades) => {
    return module.assignments
      .reduce(
        (acc, curr) =>
          acc + (curr.evaluations[0].grade ? curr.evaluations[0].grade : 0) * 0.01 * curr.weight,
        0
      )
      .toFixed(2);
  };

  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <h1 className="verticalMargin">Grades</h1>
      <div className="verticalMargin">
        <Button onClick={() => history.goBack()}>Go back</Button>
      </div>
      <WrapperTable headers={tableHeaders} ariaText="table-trainee-grades">
        <ClickableTable
          pathTo="grades/details"
          items={data.map((module) => ({
            id: module.id,
            payload: module,
            clickable: true,
            name: module.name,
            grade: calculateGrade(module),
          }))}
        />
      </WrapperTable>
    </Container>
  );
};

export default Grades;
