import { Theme } from '@material-ui/core/styles/createMuiTheme';

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    palette: {
      type: string;
      primary: {
        main: string;
      };
    };
  }
  // allow configuration using `createMuiTheme`
  interface ThemeOptions {
    palette?: {
      type: string;
      primary: {
        main: string;
      };
    };
  }
}
