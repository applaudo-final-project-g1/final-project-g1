/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { belongsTo, Model, RestSerializer, createServer, hasMany } from 'miragejs';

const { REACT_APP_API_URL_PREFIX: baseURL, REACT_APP_API_NAMESPACE: namespace } = process.env;

export default function makeServer() {
  const mockModels = {
    employee: Model.extend({
      email: '',
      firstNames: '',
      lastNames: '',
      isActive: true,
      rol: [''],
    }),

    trainee: Model.extend({
      email: '',
      firstNames: '',
      lastNames: '',
      isActive: true,
      rol: [''],
      program: belongsTo(),
    }),

    program: Model.extend({
      name: '',
      description: '',
      startDate: '',
      endDate: '',
      developers: hasMany('employee'),
      trainers: hasMany('employee'),
    }),

    module: Model.extend({
      name: '',
      description: '',
      program: belongsTo(),
    }),

    assignment: Model.extend({
      label: '',
      weight: 0,
      canSubmit: false,
      module: belongsTo(),
    }),

    evaluationScale: Model.extend({
      label: '',
      percentage: 0,
    }),

    item: Model.extend({
      description: '',
      weight: 0,
      assignment: belongsTo(),
    }),
  };

  const mockFactories = {};

  return createServer<typeof mockModels, typeof mockFactories>({
    serializers: {
      application: RestSerializer,
      program: RestSerializer.extend({
        include: ['developers', 'trainers'],
        embed: true,
      }),
      assignment: RestSerializer.extend({
        include: ['module'],
        embed: true,
      }),
      trainee: RestSerializer.extend({
        include: ['program'],
      }),
      item: RestSerializer.extend({
        include: ['assignment'],
      }),
    },

    models: mockModels,

    seeds(server) {
      const jonathan = server.create('employee', {
        firstNames: 'Jonathan',
        lastNames: 'Palma',
        email: 'jpalma@applaudostudios.com',
        isActive: true,
        rol: ['trainer'],
      });
      const osvaldo = server.create('employee', {
        firstNames: 'Osvaldo',
        lastNames: 'Escobar',
        email: 'oescobar@applaudostudios.com',
        isActive: true,
        rol: ['trainer', 'developer'],
      });
      const jose = server.create('employee', {
        firstNames: 'Jose',
        lastNames: 'Gonzales',
        email: 'jgonzalez@applaudostudios.com',
        isActive: true,
        rol: ['trainer', 'developer'],
      });
      const alejandro = server.create('employee', {
        firstNames: 'Alejandro',
        lastNames: 'Paz',
        email: 'apaz@applaudostudios.com',
        isActive: true,
        rol: ['trainer'],
      });
      const aquino = server.create('employee', {
        firstNames: 'Jose',
        lastNames: 'Aquino',
        email: 'jaquino@applaudostudios.com',
        isActive: true,
        rol: ['trainer'],
      });

      const react = server.create('program', {
        name: 'ReactJS Nov-20',
        description: 'Applaudo Trainee Program for ReactJS on November 2020',
        startDate: '2020-11-04T18:12:19.884Z',
        endDate: '2021-01-20T18:12:19.884Z',
        trainers: [osvaldo, jonathan],
        developers: [jose],
      });

      const angular = server.create('program', {
        name: 'Angular Feb-20',
        description: 'Applaudo Trainee Program for Angular on February 2020',
        startDate: '2020-02-04T18:12:19.884Z',
        endDate: '2020-05-20T18:12:19.884Z',
        trainers: [alejandro, aquino],
      });

      server.create('trainee', {
        firstNames: 'Vicente',
        lastNames: 'De Paz',
        email: 'danconec7ion@gmail.com',
        isActive: true,
        rol: ['trainee'],
        program: react,
      });
      server.create('trainee', {
        firstNames: 'Ernesto',
        lastNames: 'Hernández',
        email: '00123216@uca.edu.sv',
        isActive: true,
        rol: ['trainee'],
        program: react,
      });
      server.create('trainee', {
        firstNames: 'Emerson',
        lastNames: 'Nolasco',
        email: 'gammanolasco7@gmail.com',
        isActive: true,
        rol: ['trainee'],
        program: angular,
      });

      const rModule1 = server.create('module', {
        name: 'Fundamentals of Javascript',
        description:
          'This week is about the fundamentals of JavaScript, here we are going to learn about concepts that serve as the basis for the knowledge of the following weeks. For example, we will start with the topics of variables and values and then move on to the different types and data structures that exist in the language.',
        program: react,
      });

      const rModule2 = server.create('module', {
        name: 'Object Oriented Programming',
        description:
          'Maybe you have heard about these programming terms before: classes, instance of a class, inheritance, object oriented programming. If not, do not worry, we got you covered. This week will be all about getting a fast and efficient undestanding of these programming paradigms.',
        program: react,
      });

      const aModule1 = server.create('module', {
        name: 'Fundamentals of Javascript',
        description:
          'This week is about the fundamentals of JavaScript, here we are going to learn about concepts that serve as the basis for the knowledge of the following weeks. For example, we will start with the topics of variables and values and then move on to the different types and data structures that exist in the language.',
        program: angular,
      });

      const aModule2 = server.create('module', {
        name: 'Angular',
        description:
          'In this week we are going to talk about the Google javascript framework: Angular, before called: AngularJS.',
        program: angular,
      });

      server.create('evaluationScale', {
        label: 'Excelent',
        weight: 10,
      });

      server.create('evaluationScale', {
        label: 'Above average',
        weight: 80,
      });

      server.create('evaluationScale', {
        label: 'Great job',
        weight: 60,
      });

      server.create('evaluationScale', {
        label: 'Needs improvement',
        weight: 40,
      });

      server.create('evaluationScale', {
        label: 'Deficient',
        weight: 20,
      });

      const oralQuizJsReact = server.create('assignment', {
        label: 'Oral quiz',
        weight: 50,
        module: rModule1,
        canSubmit: false,
      });

      server.create('item', {
        description: 'What is javascript?',
        weight: 10,
        assignment: oralQuizJsReact,
      });

      server.create('item', {
        description: 'What is hoisting?',
        weight: 20,
        assignment: oralQuizJsReact,
      });

      server.create('assignment', {
        label: 'Homework review',
        weight: 50,
        module: rModule1,
        canSubmit: true,
      });

      server.create('assignment', {
        label: 'Homework review',
        weight: 50,
        module: rModule1,
        canSubmit: true,
      });

      server.create('assignment', {
        label: 'Oral quiz',
        weight: 60,
        module: rModule2,
        canSubmit: false,
      });

      server.create('assignment', {
        label: 'Homework review',
        weight: 40,
        module: rModule2,
        canSubmit: true,
      });

      server.create('assignment', {
        label: 'Oral quiz',
        weight: 50,
        module: aModule1,
        canSubmit: false,
      });

      server.create('assignment', {
        label: 'Homework review',
        weight: 50,
        module: aModule1,
        canSubmit: true,
      });

      server.create('assignment', {
        label: 'Oral quiz',
        weight: 60,
        module: aModule2,
        canSubmit: false,
      });

      server.create('assignment', {
        label: 'Homework review',
        weight: 40,
        module: aModule2,
        canSubmit: true,
      });
    },

    routes() {
      this.passthrough();
      this.urlPrefix = baseURL ?? '';

      this.namespace = namespace ?? '';

      this.get('/rols');
      this.get('/rols/:id');

      this.get('/employees', (schema, request) => {
        const qp = request.queryParams;
        const offset = parseInt(qp.offset ?? 0, 10);
        const limit = parseInt(qp.limit ?? 10, 10);
        const end = offset + limit;
        const res = schema.all('employee');
        res.models = res.models.slice(offset, end);

        return res;
      });
      this.get('/employees/:id');
      // this.post('/auth/login', (schema, request): any => {
      //   const { email } = JSON.parse(request.requestBody);
      //   let employee = schema.findBy('employee', { email });
      //   if (!employee) {
      //     employee = schema.findBy('trainee', { email });
      //   }

      //   return employee;
      // });

      this.get('/trainees', (schema, request) => {
        const qp = request.queryParams;
        const offset = parseInt(qp.offset ?? 0, 10);
        const limit = parseInt(qp.limit ?? 10, 10);
        const end = offset + limit;
        const res = schema.all('trainee');
        res.models = res.models.slice(offset, end);

        return res;
      });
      this.get('/trainees/:id');

      this.get('/programs', (schema, request) => {
        const qp = request.queryParams;
        const offset = parseInt(qp.offset ?? 0, 10);
        const limit = parseInt(qp.limit ?? 10, 10);
        const end = offset + limit;
        const res = schema.all('program');
        res.models = res.models.slice(offset, end);

        return res;
      });
      this.get('/programs/:id');
      this.get('/programs/:id/modules', (schema, request) => {
        const { id } = request.params;
        const modules = schema.where('module', { programId: id } as any);

        return modules;
      });

      this.get('/modules');
      this.get('/modules/:id');
      this.get('/modules/:id/assignments', (schema, request) => {
        const { id } = request.params;
        const assignments = schema.where('assignment', { moduleId: id } as any);

        return assignments;
      });
      this.post('/modules', (schema, request) => {
        const attrs = JSON.parse(request.requestBody);

        return schema.create('module', attrs);
      });
      this.patch('/modules/:id', (schema, request): any => {
        const { id } = request.params;
        const attrs = JSON.parse(request.requestBody);
        const module = schema.find('module', id);

        return module?.update(attrs);
      });
      this.del('/modules/:id', (schema, request): any => {
        const { id } = request.params;
        const item = schema.findBy('module', { id } as any);

        item?.destroy();
      });

      this.get('/assignments');
      this.get('/assignments/:id');
      this.post('/assignments', (schema, request) => {
        const attrs = JSON.parse(request.requestBody);

        return schema.create('assignment', attrs);
      });

      this.del('/assignments/:id', (schema, request): any => {
        const { id } = request.params;
        const item = schema.findBy('assignment', { id } as any);

        item?.destroy();
      });
      this.get('/assignments/:id/items', (schema, request) => {
        const { id } = request.params;
        const items = schema.where('item', { assignmentId: id } as any);

        return items;
      });
      this.post('/assignments/:id/items', (schema, request) => {
        const { id } = request.params;
        const attrs = JSON.parse(request.requestBody);
        attrs.assignmentId = id;

        return schema.create('item', attrs);
      });
      this.patch('/assignments/:aid/items/:id', (schema, request): any => {
        const { id } = request.params;
        const attrs = JSON.parse(request.requestBody);
        const module = schema.find('item', id);

        return module?.update(attrs);
      });
      this.get('/assignments/:id/items/:itemId', (schema, request): any => {
        const { id, itemId } = request.params;
        const item = schema.findBy('item', { id: itemId, assignmentId: id } as any);

        return item;
      });
      this.del('/assignments/:id/items/:itemId', (schema, request): any => {
        const { id, itemId } = request.params;
        const item = schema.findBy('item', { id: itemId, assignmentId: id } as any);

        item?.destroy();
      });

      this.get('/evaluation-scales');
      this.get('/evaluation-scales/:id');
    },
  });
}
