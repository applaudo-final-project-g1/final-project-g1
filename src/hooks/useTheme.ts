import { Dispatch, SetStateAction, useContext } from 'react';

import { Theme, useTheme } from '@material-ui/core/styles';

import { ThemeProviderDispatchContext } from 'context/providers/AppTheme';

const useThemeProviderDispatch = (): Dispatch<SetStateAction<string>> => {
  const context = useContext(ThemeProviderDispatchContext);
  if (context === undefined) {
    throw new Error('useThemeProviderDispatch must be used within ThemeProviderDispatchContext');
  }
  return context;
};

const useThemeContext = (): Theme => {
  const context = useTheme();
  if (context === undefined) {
    throw new Error('useThemeContext must be used within ThemeProvider');
  }
  return context;
};

const useThemeProvider = (): [Theme, Dispatch<SetStateAction<string>>] => [
  useThemeContext(),
  useThemeProviderDispatch(),
];

export default useThemeProvider;
