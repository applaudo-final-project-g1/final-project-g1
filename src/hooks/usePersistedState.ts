import { Dispatch, SetStateAction, useEffect, useState } from 'react';

import CryptoJS, { AES } from 'crypto-js';

const { REACT_APP_SECRET: secret } = process.env;

type ResponseState<T> = [T, Dispatch<SetStateAction<T>>];

const usePersistedState = <T>(key: string, defaultState: T): ResponseState<T> => {
  const [state, setState] = useState<T>(() => {
    if (key && secret) {
      const savedState = window.localStorage.getItem(key);
      try {
        if (!savedState) {
          return defaultState;
        }
        const decryptInfo = AES.decrypt(JSON.parse(savedState), secret).toString(CryptoJS.enc.Utf8);
        return JSON.parse(decryptInfo);
      } catch (e) {
        return defaultState;
      }
    }
    return defaultState;
  });

  useEffect(() => {
    if (secret) {
      const encryptInfo = AES.encrypt(JSON.stringify(state), secret).toString();
      window.localStorage.setItem(key, JSON.stringify(encryptInfo));
    }
  }, [key, state]);

  return [state, setState];
};

export default usePersistedState;
