import { Dispatch, SetStateAction, useContext } from 'react';

import { UserDispatchContext, UserStateContext } from 'context/providers/UserProvider';
import { IUser } from 'interfaces/IUser';

const useUserState = (): IUser => {
  const context = useContext(UserStateContext);
  if (context === undefined) {
    throw new Error('useUserDispatch must be used within UserProvider');
  }
  return context;
};

const useUserDispatch = (): Dispatch<SetStateAction<IUser>> => {
  const context = useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error('useUserDispatch must be used within UserProvider');
  }
  return context;
};

const useUser = (): [IUser, Dispatch<SetStateAction<IUser>>] => [useUserState(), useUserDispatch()];

export default useUser;
