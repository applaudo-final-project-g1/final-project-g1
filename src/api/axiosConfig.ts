import axios from 'axios';
import CryptoJS, { AES } from 'crypto-js';

const {
  REACT_APP_API_BASE_URL: baseURL,
  REACT_APP_USER_LOCALSTORAGE_KEY: userKey,
  REACT_APP_SECRET: secret,
} = process.env;

const axiosInstance = axios.create({
  baseURL,
  timeout: 35000,
});

axiosInstance.interceptors.request.use(
  (config) => {
    if (userKey) {
      const encryptedData = window.localStorage.getItem(userKey);
      if (encryptedData && secret) {
        const decryptInfo = AES.decrypt(JSON.parse(encryptedData), secret).toString(
          CryptoJS.enc.Utf8
        );
        const token: string | undefined = JSON.parse(decryptInfo ?? '{}')?.jwt;
        if (token) {
          // eslint-disable-next-line no-param-reassign
          config.headers = {
            Authorization: `Bearer ${token}`,
          };
        }
      }
    }
    return config;
  },
  (error) => Promise.reject(error)
);

export default axiosInstance;
