import axiosInstance from './axiosConfig';

const fetcher = <T>(url: string): Promise<T> =>
  axiosInstance
    .get(url)
    .then((response) => {
      if (response.statusText === 'OK') {
        return response.data;
      }
      throw new Error(response.status.toString());
    })
    .catch(({ response }) => {
      throw new Error(response.status);
    });

export { fetcher as default };
