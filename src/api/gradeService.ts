import { AxiosResponse } from 'axios';

import { IEvaluation } from 'interfaces/IEvaluation';

import axiosInstance from './axiosConfig';

type GradeType = {
  evaluationScaleId: number;
  assignmentItemId: number;
  feedback: string;
};

interface IGrade {
  id: number;
  assignmentItemId: number;
  weight: number;
  evaluationId: number;
  percentage: string;
  feedback: string;
}

const grade = (id: number, data: GradeType): Promise<IGrade> =>
  axiosInstance
    .put<GradeType, AxiosResponse<IGrade>>(`/evaluations/${id}/grades`, data)
    .then((res) => res.data);

const finish = (id: number): Promise<IEvaluation> =>
  axiosInstance
    .patch<never, AxiosResponse<IEvaluation>>(`/evaluations/${id}/finish`)
    .then((res) => res.data);

export { grade as default, finish };
