/* eslint-disable no-console */
import { AxiosResponse } from 'axios';

import IItem from 'interfaces/IItem';

import axiosInstance from './axiosConfig';

type AddType = { description: string; weight: number };

type UpdateType = Partial<AddType>;

const addAssignmentItem = (assignmentId: string, data: AddType): Promise<IItem> =>
  axiosInstance
    .post<AddType, AxiosResponse<IItem>>(`/assignments/${assignmentId}/items`, data)
    .then((res) => res.data);

const updateAssignmentItem = (
  assignmentId: string,
  itemId: number,
  data: UpdateType
): Promise<IItem> =>
  axiosInstance
    .patch<UpdateType, AxiosResponse<IItem>>(`/assignments/${assignmentId}/items/${itemId}`, data)
    .then((response) => response.data);

const deleteAssignmentItem = (assignmentId: string, itemId: number): Promise<IItem> =>
  axiosInstance
    .delete<never, AxiosResponse<IItem>>(`/assignments/${assignmentId}/items/${itemId}`)
    .then((res) => res.data);

export { addAssignmentItem, updateAssignmentItem, deleteAssignmentItem };
