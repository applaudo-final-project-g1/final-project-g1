import { AxiosResponse } from 'axios';

import { IAssignmentWithEvaluations } from 'interfaces/IAssignment';
import IItem from 'interfaces/IItem';
import IModule from 'interfaces/IModule';

import axiosInstance from './axiosConfig';
import fetcher from './axiosFetcher';

type AddType = { name: string; description: string; programId: string };
type EditType = { name: string; description: string };

type PromiseAllType = [IAssignmentWithEvaluations, IItem[]];

const add = (data: AddType): Promise<IModule> =>
  axiosInstance.post<AddType, AxiosResponse<IModule>>('/modules', data).then((res) => res.data);

const getAssignmentsWithEvaluationsAndItems = (...urls: string[]): Promise<PromiseAllType> => {
  const evaluationsPromise = () => fetcher<IAssignmentWithEvaluations>(urls[0]);
  const itemsPromise = () => fetcher<IItem[]>(urls[1]);

  return Promise.all([evaluationsPromise(), itemsPromise()]);
};

const edit = (id: number, data: EditType): Promise<IModule> =>
  axiosInstance
    .patch<EditType, AxiosResponse<IModule>>(`/modules/${id}`, data)
    .then((res) => res.data);

const deleteModule = (id: number): Promise<IModule> =>
  axiosInstance.delete<never, AxiosResponse<IModule>>(`/modules/${id}`).then((res) => res.data);

export { add as default, getAssignmentsWithEvaluationsAndItems, edit, deleteModule };
