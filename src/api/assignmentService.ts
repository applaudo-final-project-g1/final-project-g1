import { AxiosResponse } from 'axios';

import { IAssignment } from 'interfaces/IAssignment';
import { IEvaluation } from 'interfaces/IEvaluation';

import axiosInstance from './axiosConfig';

type AddType = { label: string; weight: number; moduleId: string; canSubmit: boolean };

type UpdateType = Partial<AddType>;

const addAssignment = (data: AddType): Promise<IAssignment> =>
  axiosInstance
    .post<AddType, AxiosResponse<IAssignment>>('/assignments', data)
    .then((res) => res.data);

const updateAssignment = (id: string, data: UpdateType): Promise<IAssignment> =>
  axiosInstance
    .patch<UpdateType, AxiosResponse<IAssignment>>(`/assignments/${id}`, data)
    .then((response) => response.data);

const deleteAssigment = (id: number): Promise<IAssignment> =>
  axiosInstance
    .delete<never, AxiosResponse<IAssignment>>(`/assignments/${id}`)
    .then((res) => res.data);

const generateEvaluations = (id: string): Promise<IEvaluation[]> =>
  axiosInstance.post(`evaluations/generate`, { assignmentId: id }).then((res) => res.data);

export { addAssignment, updateAssignment, deleteAssigment, generateEvaluations };
