import { AxiosResponse } from 'axios';

import axiosInstance from './axiosConfig';

type LoginDataType = { googleToken: string };
type LoginResponse = { accessToken: string };

const login = (data: LoginDataType): Promise<AxiosResponse<LoginResponse>> =>
  axiosInstance.post('/auth/login', data).then((res) => res);

const logout = (): Promise<AxiosResponse<void>> => axiosInstance.delete('auth/logout');

export { login, logout };
