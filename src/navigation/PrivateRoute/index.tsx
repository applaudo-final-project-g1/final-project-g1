/* eslint-disable no-constant-condition */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { Redirect, Route, RouteComponentProps, useLocation } from 'react-router-dom';

import RouteAuthorization from 'components/RouteAuthorization';
import useUser from 'hooks/useUser';
import Routes from 'navigation/Routes';

type IPrivateRoute = {
  component: React.ElementType;
  layout?: React.ElementType;
  exact: boolean;
  path: Routes;
};

const PrivateRoute = ({
  component: Component,
  layout: Layout,
  exact,
  path,
}: IPrivateRoute): JSX.Element => {
  const location = useLocation();
  const [user] = useUser();

  return (
    <Route
      exact={exact}
      path={path}
      component={(props: RouteComponentProps) => {
        if (user.email) {
          if (Layout) {
            return (
              <RouteAuthorization
                route={path}
                yes={
                  <Layout>
                    <Component {...props} />
                  </Layout>
                }
                no={
                  <Redirect
                    to={{
                      pathname: Routes.HOME,
                      state: { from: location },
                    }}
                  />
                }
              />
            );
          }
          return (
            <RouteAuthorization
              route={path}
              yes={<Component {...props} />}
              no={
                <Redirect
                  to={{
                    pathname: Routes.HOME,
                    state: { from: location },
                  }}
                />
              }
            />
          );
        }
        return (
          <Redirect
            to={{
              pathname: Routes.LOGIN,
              state: { from: location },
            }}
          />
        );
      }}
    />
  );
};

PrivateRoute.defaultProps = {
  layout: undefined,
};

export default PrivateRoute;
