import { RolesEnum } from 'interfaces/IEmployee';

import Routes from './Routes';

export enum ModuleEnum {
  routes,
  module,
  assignment,
  item,
  evaluation,
}

export enum PermissionEnum {
  read,
  add,
  edit,
  delete,
}

export type RulesType = {
  [k in RolesEnum]: {
    [ModuleEnum.routes]: Routes[];
    [ModuleEnum.module]: PermissionEnum[];
    [ModuleEnum.assignment]: PermissionEnum[];
    [ModuleEnum.item]: PermissionEnum[];
    [ModuleEnum.evaluation]: PermissionEnum[];
  };
};

const rules: RulesType = {
  [RolesEnum.developer]: {
    [ModuleEnum.routes]: [
      Routes.HOME,
      Routes.PROGRAM,
      Routes.MODULE,
      Routes.ASSIGMENT,
      Routes.NEW_ASSIGMENT,
      Routes.EVALUATION,
      Routes.PROFILE,
    ],
    [ModuleEnum.module]: [
      PermissionEnum.add,
      PermissionEnum.edit,
      PermissionEnum.delete,
      PermissionEnum.read,
    ],
    [ModuleEnum.assignment]: [
      PermissionEnum.add,
      PermissionEnum.edit,
      PermissionEnum.delete,
      PermissionEnum.read,
    ],
    [ModuleEnum.item]: [
      PermissionEnum.add,
      PermissionEnum.edit,
      PermissionEnum.delete,
      PermissionEnum.read,
    ],
    [ModuleEnum.evaluation]: [PermissionEnum.read],
  },
  [RolesEnum.manager]: {
    [ModuleEnum.routes]: [Routes.HOME, Routes.PROGRAM],
    [ModuleEnum.module]: [PermissionEnum.read],
    [ModuleEnum.assignment]: [],
    [ModuleEnum.item]: [],
    [ModuleEnum.evaluation]: [],
  },
  [RolesEnum.trainer]: {
    [ModuleEnum.routes]: [
      Routes.HOME,
      Routes.PROGRAM,
      Routes.MODULE,
      Routes.ASSIGMENT,
      Routes.EVALUATION,
      Routes.PROFILE,
    ],
    [ModuleEnum.module]: [PermissionEnum.read],
    [ModuleEnum.assignment]: [PermissionEnum.read],
    [ModuleEnum.item]: [PermissionEnum.read],
    [ModuleEnum.evaluation]: [PermissionEnum.read, PermissionEnum.edit],
  },
  [RolesEnum.trainee]: {
    [ModuleEnum.routes]: [
      Routes.HOME,
      Routes.PROGRAM,
      Routes.MODULE,
      Routes.ASSIGMENT,
      Routes.EVALUATION,
      Routes.GRADES,
      Routes.PROFILE,
      Routes.GRADES_DETAILS,
    ],
    [ModuleEnum.module]: [PermissionEnum.read],
    [ModuleEnum.assignment]: [PermissionEnum.read],
    [ModuleEnum.item]: [PermissionEnum.read],
    [ModuleEnum.evaluation]: [PermissionEnum.read],
  },
};

export default rules;
