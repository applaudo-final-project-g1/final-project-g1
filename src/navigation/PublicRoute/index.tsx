/* eslint-disable no-constant-condition */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { Redirect, Route, RouteComponentProps, useLocation } from 'react-router-dom';

import useUser from 'hooks/useUser';
import Routes from 'navigation/Routes';

type IPublicRoute = {
  component: React.ElementType;
  layout?: React.ElementType;
  exact: boolean;
  path: string;
};

const PublicRoute = ({
  component: Component,
  layout: Layout,
  exact,
  path,
}: IPublicRoute): JSX.Element => {
  const location = useLocation();
  const [user] = useUser();

  return (
    <Route
      exact={exact}
      path={path}
      component={(props: RouteComponentProps) => {
        if (!user.email) {
          if (Layout) {
            return (
              <Layout>
                <Component {...props} />
              </Layout>
            );
          }
          return <Component {...props} />;
        }
        return (
          <Redirect
            to={{
              pathname: Routes.HOME,
              state: { from: location },
            }}
          />
        );
      }}
    />
  );
};

PublicRoute.defaultProps = {
  layout: undefined,
};

export default PublicRoute;
