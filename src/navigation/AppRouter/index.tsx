import React, { Suspense, lazy } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import DrawerLayout from 'components/layouts/DrawerLayout';
import Loading from 'components/Loading';
import PrivateRoute from 'navigation/PrivateRoute';
import PublicRoute from 'navigation/PublicRoute';
import Routes from 'navigation/Routes';

const Home = lazy(() => import('pages/Home'));
const Login = lazy(() => import('pages/Login'));
const ProgramDetail = lazy(() => import('pages/ProgramDetail'));
const ModuleDetail = lazy(() => import('pages/ModuleDetail'));
const AssignmentDetail = lazy(() => import('pages/AssignmentDetail'));
const EvaluationDetail = lazy(() => import('pages/EvaluationDetail'));
const Grades = lazy(() => import('pages/Grades'));
const Profile = lazy(() => import('pages/Profile'));
const GradesDetails = lazy(() => import('pages/GradesDetails'));
const NotFound = lazy(() => import('pages/NotFound'));

const AppRouter = (): JSX.Element => (
  <Router>
    <Suspense fallback={<Loading />}>
      <Switch>
        <PublicRoute exact path={Routes.LOGIN} component={Login} />
        <PrivateRoute exact layout={DrawerLayout} path={Routes.HOME} component={Home} />
        <PrivateRoute exact layout={DrawerLayout} path={Routes.GRADES} component={Grades} />
        <PrivateRoute exact path={Routes.GRADES_DETAILS} component={GradesDetails} />
        <PrivateRoute exact layout={DrawerLayout} path={Routes.PROGRAM} component={ProgramDetail} />
        <PrivateRoute exact layout={DrawerLayout} path={Routes.MODULE} component={ModuleDetail} />
        <PrivateRoute
          exact
          layout={DrawerLayout}
          path={Routes.ASSIGMENT}
          component={AssignmentDetail}
        />
        <PrivateRoute exact path={Routes.NEW_ASSIGMENT} component={AssignmentDetail} />
        <PrivateRoute exact path={Routes.EVALUATION} component={EvaluationDetail} />
        <PrivateRoute exact path={Routes.PROFILE} component={Profile} layout={DrawerLayout} />
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </Suspense>
  </Router>
);

export default AppRouter;
