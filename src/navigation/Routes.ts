enum Routes {
  HOME = '/',
  LOGIN = '/login',
  PROGRAM = '/programs/:programId',
  MODULE = '/modules/:moduleId',
  NEW_ASSIGMENT = '/modules/:moduleId/assignment',
  ASSIGMENT = '/modules/:moduleId/assignment/:assignmentId',
  EVALUATION = '/assignments/:assignmentId/evaluations',
  GRADES = '/trainees/grades',
  PROFILE = '/me',
  GRADES_DETAILS = '/trainees/grades/details',
}

export default Routes;
