import { ThemeOptions } from '@material-ui/core';

const lightTheme: ThemeOptions = {
  palette: {
    type: 'light',
    primary: {
      main: '#FF4040',
    },
    secondary: {
      main: '#011625',
    },
  },
  typography: {
    fontFamily: ['Raleway', 'Roboto', '"Helvetica Neue"', 'sans-serif'].join(','),
  },
};

export default lightTheme;
