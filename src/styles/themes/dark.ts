import { ThemeOptions } from '@material-ui/core';

const darkTheme: ThemeOptions = {
  palette: {
    type: 'dark',
    primary: {
      main: '#011625',
    },
  },
};

export default darkTheme;
