import React, { createContext, Dispatch, ReactNode, SetStateAction } from 'react';

import usePersistedState from 'hooks/usePersistedState';
import { IUser } from 'interfaces/IUser';

const { REACT_APP_USER_LOCALSTORAGE_KEY: userStorageKey } = process.env;

const UserStateContext = createContext<IUser | undefined>(undefined);
const UserDispatchContext = createContext<Dispatch<SetStateAction<IUser>> | undefined>(undefined);

type Props = {
  children: ReactNode;
};

const UserProvider = ({ children }: Props): JSX.Element => {
  const [state, dispatch] = usePersistedState<IUser>(userStorageKey ?? '', {} as IUser);

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>{children}</UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
};

export { UserProvider as default, UserStateContext, UserDispatchContext };
