import React, { createContext, Dispatch, ReactNode, SetStateAction } from 'react';

import { createMuiTheme, ThemeProvider } from '@material-ui/core';

import usePersistedState from 'hooks/usePersistedState';
import darkTheme from 'styles/themes/dark';
import lightTheme from 'styles/themes/light';

const { REACT_APP_THEME_LOCALSTORAGE_KEY: themeStorageKey } = process.env;

const ThemeProviderDispatchContext = createContext<Dispatch<SetStateAction<string>> | undefined>(
  undefined
);

type Props = {
  children: ReactNode;
};

const defaultState = 'light';

const AppThemeProvider = ({ children }: Props): JSX.Element => {
  const [theme, setTheme] = usePersistedState<string>(themeStorageKey ?? '', defaultState);
  const appliedTheme = createMuiTheme(theme === 'light' ? lightTheme : darkTheme);

  return (
    <ThemeProvider theme={appliedTheme}>
      <ThemeProviderDispatchContext.Provider value={setTheme}>
        {children}
      </ThemeProviderDispatchContext.Provider>
    </ThemeProvider>
  );
};

export { AppThemeProvider as default, ThemeProviderDispatchContext };
