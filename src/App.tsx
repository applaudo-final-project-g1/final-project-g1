import React from 'react';

import ErrorBoundary from 'components/ErrorBoundary';
import AppThemeProvider from 'context/providers/AppTheme';
import UserProvider from 'context/providers/UserProvider';
import AppRouter from 'navigation/AppRouter';

const App = (): JSX.Element => {
  return (
    <ErrorBoundary>
      <AppThemeProvider>
        <UserProvider>
          <AppRouter />
        </UserProvider>
      </AppThemeProvider>
    </ErrorBoundary>
  );
};

export default App;
