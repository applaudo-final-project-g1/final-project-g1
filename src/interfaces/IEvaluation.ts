import ITrainee from './ITrainee';

export interface IEvaluation {
  id: number;
  trainee: ITrainee;
  assignmentId: number;
  date: Date | null;
  attachment: string | null;
  finishedAt: Date | null;
  grade: number | null;
}
