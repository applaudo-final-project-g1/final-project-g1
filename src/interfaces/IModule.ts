interface IModule {
  id: number;
  name: string;
  description: string;
  programId: string;
}

export default IModule;
