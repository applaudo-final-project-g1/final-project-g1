import { RolesEnum } from './IEmployee';

interface ITrainee {
  id: number;
  email: string;
  firstNames: string | null;
  lastNames: string | null;
  isActive: boolean;
  roles?: RolesEnum[];
  programId: string;
}

export default ITrainee;
