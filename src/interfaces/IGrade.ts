import IItem from './IItem';
import { IAssignment } from 'interfaces/IAssignment';
import { IEvaluation } from 'interfaces/IEvaluation';
import IEvaluationScale from 'interfaces/IEvaluationScale';

export interface IGrade {
  id: number;
  assignmentItemId: number;
  weight: number;
  evaluationId: number;
  scale?: IEvaluationScale;
  feedback: string;
}

interface IEvaluationWithGrades extends IEvaluation {
  grades: IGrade[];
}

export interface IAssignmentGrade extends IAssignment {
  evaluations: IEvaluationWithGrades[];
  assignmentItems: IItem[];
}

export interface ITraineeGrades {
  assignments: IAssignmentGrade[];
  id: number;
  name: string;
  description: string;
  programId: number;
}
