import { SvgIconTypeMap } from '@material-ui/core';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

export interface IMenuItem {
  name: string;
  route: string;
  icon: OverridableComponent<SvgIconTypeMap>;
}
