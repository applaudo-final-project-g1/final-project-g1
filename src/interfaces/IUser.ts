import { RolesEnum } from './IEmployee';

export interface IUser {
  userId: number;
  email: string;
  roles: RolesEnum[];
  iat: number;
  exp: number;
  jwt: string;
}
