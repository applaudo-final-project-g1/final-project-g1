export interface ITableRow<T> {
  id: number | string;
  payload: T;
  clickable: boolean;
}
