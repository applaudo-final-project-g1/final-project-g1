interface IEvaluationScale {
  id: number;
  label: string;
  percentage: string;
}

export default IEvaluationScale;
