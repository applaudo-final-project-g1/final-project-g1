interface IItem {
  id: number;
  description: string;
  weight: number;
  assignmentId: number;
}

export default IItem;
