export enum RolesEnum {
  developer = 'developer',
  manager = 'manager',
  trainer = 'trainer',
  trainee = 'trainee',
}

interface IEmployee {
  id: number;
  email: string;
  firstNames: string;
  lastNames: string;
  isActive: boolean;
  roles: RolesEnum[];
}

export default IEmployee;
