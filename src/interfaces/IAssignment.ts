import { IEvaluation } from './IEvaluation';
import IModule from './IModule';

export interface IAssignment {
  id: number;
  label: string;
  weight: number;
  moduleId: IModule;
  canSubmit: boolean;
}

export interface IAssignmentWithEvaluations extends IAssignment {
  evaluations: IEvaluation[];
}
