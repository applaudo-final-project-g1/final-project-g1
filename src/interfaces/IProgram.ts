import IEmployee from './IEmployee';
import IModule from './IModule';

interface IProgram {
  id: number;
  name: string;
  description: string;
  startDate: string;
  endDate: string;
  developers: IEmployee[];
  trainers: IEmployee[];
}

export interface IProgramWithModules extends IProgram {
  modules: IModule[];
}

export default IProgram;
